/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once
namespace Solid
{
  struct Material
  {
    float density;
    float restitution;
    float friction;
    Material(float density_, float restitution_, float friction_) : density(density_), restitution(restitution_), friction(friction_) {}
    Material() : density(1.0f), restitution(0.4f), friction(0.4f) {}
  };
}