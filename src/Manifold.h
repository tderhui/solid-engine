/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once

#include "Vec2.h"
#include "Face.h"

namespace Solid
{
  class RigidBody;

  class Manifold {
  public:
    enum class Type
    {
      PolyToPoly,
      PolyToCircle,
      CircleToPoly,
      CircleToCircle
    };
  
    Manifold();

    void ApplyCollisionImpulse(float dt);
    void PositionalCorrection();

    bool collision;
    char contactCount;
    float collisionDepth;
    Vec2 collisionNormal;
    Vec2 contacts[2];
    bool flip;
    RigidBody *bodyA;
    RigidBody *bodyB;

    // accumulated impulse. this is used to achieve more accurate results
    float accumulatedImpulseMagnitude[2];

  };
}
