/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/
#include "AABB.h"
#include "RigidBody.h"
#include <algorithm>

namespace Solid
{
  AABB::AABB(Vec2 min, Vec2 max) : m_min(min), m_max(max) {}

  bool AABB::Contains(Vec2 point)
  {
    if ((point.x > m_min.x && point.x < m_max.x)
      && (point.y > m_min.y && point.y < m_max.y))
      return true;

    return false;
  }

  bool AABB::Contains(const AABB &rhs)
  {
    if ((m_min.x < rhs.m_min.x && m_max.x > rhs.m_max.x)
      && (m_min.y < rhs.m_min.y && m_max.y > rhs.m_max.y))
      return true;

    return false;
  }

  bool AABB::Contains(RigidBody *rhs)
  {
    AABB otherAABB = rhs->GetBoundingBox();

    return Contains(otherAABB);
  }

  // REQ_TESTING
  bool AABB::Intersects(const AABB &rhs)
  {
    // left right check
    if (m_max.x < rhs.m_min.x ||
      rhs.m_max.x < m_min.x)
      return false;

    // up down check
    if (m_max.y < rhs.m_min.y ||
      rhs.m_max.y < m_min.y)
      return false;

    return true;
  }

  // TODO_DERHUI i totally forgot how to do this
  bool AABB::Intersects(RigidBody *rhs)
  {
    AABB otherAABB = rhs->GetBoundingBox();

    return Contains(otherAABB);
  }

  AABB AABB::operator+(const AABB &rhs)
  {
    // get min x, min y, max x, max y
    float minX = std::min(m_min.x, rhs.m_min.x);
    float minY = std::min(m_min.y, rhs.m_min.y);

    float maxX = std::max(m_max.x, rhs.m_max.x);
    float maxY = std::max(m_max.y, rhs.m_max.y);

    return AABB(Vec2(minX, minY), Vec2(maxX, maxY));
  }

  float AABB::GetVolume()
  {
    return (m_max.x - m_min.x) * (m_max.y - m_min.y);
  }
}