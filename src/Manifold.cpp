/******************************************************************************

 Copyright 2015 Tai Der Hui. All rights reserved.
 Use of this source code is governed by a BSD-style license that can be
 found in the LICENSE file.

******************************************************************************/

#include "Manifold.h"
#include <algorithm>
#include "RigidBody.h"
#include "Shape.h"

namespace Solid
{
  static const float CORRECTION = 0.4f;

  Manifold::Manifold() : collision(false), collisionDepth(0), collisionNormal(0, 0), contactCount(0), flip(false)
  {
    accumulatedImpulseMagnitude[0] = 0.0f;
    accumulatedImpulseMagnitude[1] = 0.0f;
  }

  void Manifold::ApplyCollisionImpulse(float dt)
  {
    // get minimum restitution
    float restitution = std::min(bodyA->material.restitution, bodyB->material.restitution);

    // set variables for the two bodies' mass (accounting for fixed)
    float AInverseMass = bodyA->GetFixed() ? 0 : bodyA->massData.inverseMass;
    float BInverseMass = bodyB->GetFixed() ? 0 : bodyB->massData.inverseMass;

    // set variables for the inertia (accounting for fixed)
    float AInverseInertia = bodyA->GetFixed() ? 0 : bodyA->massData.inverseInertia;
    float BInverseInertia = bodyB->GetFixed() ? 0 : bodyB->massData.inverseInertia;

    // account for rotation locked
    if (bodyA->GetRotationLocked())
      AInverseInertia = 0;
    if (bodyB->GetRotationLocked())
      BInverseInertia = 0;

    for (int i = 0; i < contactCount; ++i)
    {
      // Calculate radii from COM to contact
      Vec2 bodyACenter = bodyA->GetOwner()->transform.GetMatrix3() * (bodyA->GetShape()->center);
      Vec2 bodyBCenter = bodyB->GetOwner()->transform.GetMatrix3() * (bodyB->GetShape()->center);

      Vec2 bodyAToContact = contacts[i] - bodyACenter;
      Vec2 bodyBToContact = contacts[i] - bodyBCenter;

      // Get velocity including rotational components
      Vec2 ARotationalVelocity = Cross(bodyA->angularVelocity, bodyAToContact);
      Vec2 ATotalVelocity = bodyA->velocity + ARotationalVelocity;

      Vec2 BRotationalVelocity = Cross(bodyB->angularVelocity, bodyBToContact);
      Vec2 BTotalVelocity = bodyB->velocity + BRotationalVelocity;

      // Relative velocity
      Vec2 relativeVelocity = BTotalVelocity - ATotalVelocity;

      // Relative velocity along the normal
      float velAlongNormal = relativeVelocity * collisionNormal;

      // Do not resolve if velocities are separating
      if (velAlongNormal > 0)
        continue;

      // Calculate impulse scalar
      float magnitude = -(1 + restitution) * velAlongNormal;
      float rotationalDivisors = AInverseInertia * (bodyAToContact.Cross(collisionNormal)) * (bodyAToContact.Cross(collisionNormal));
      rotationalDivisors += BInverseInertia * (bodyBToContact.Cross(collisionNormal)) * (bodyBToContact.Cross(collisionNormal));
      magnitude /= ((AInverseMass + BInverseMass) + rotationalDivisors) * contactCount;

      // some magic happens here
      float prevMagnitude = accumulatedImpulseMagnitude[i];
      accumulatedImpulseMagnitude[i] = std::max(magnitude + accumulatedImpulseMagnitude[i], 0.0f);
      magnitude = accumulatedImpulseMagnitude[i] - prevMagnitude;

      // Apply impulse
      Vec2 impulse = collisionNormal * magnitude;

      // linear part
      bodyA->velocity -= impulse * AInverseMass;
      bodyB->velocity += impulse * BInverseMass;

      // rotational part
      bodyA->angularVelocity -= AInverseInertia * bodyAToContact.Cross(impulse);
      float added = BInverseInertia * bodyBToContact.Cross(impulse);
      bodyB->angularVelocity += added;

      // Calculate friction vector
      Vec2 newARotationalVelocity = Cross(bodyA->angularVelocity, bodyAToContact);
      Vec2 newAVelocity = bodyA->velocity + newARotationalVelocity;

      Vec2 newBRotationalVelocity = Cross(bodyB->angularVelocity, bodyBToContact);
      Vec2 newBVelocity = bodyB->velocity + BRotationalVelocity;

      Vec2 newRV = newBVelocity - newAVelocity;

      Vec2 tangent = newRV - (collisionNormal * (newRV * collisionNormal));
      tangent.Normalize();

      float tangentMagnitude = -1.0f * (newRV * tangent);
      tangentMagnitude /= ((AInverseMass + BInverseMass) + rotationalDivisors);
      tangentMagnitude /= contactCount;

      // calculate the average friction coeffecient 
      float mu;
      if (bodyA->material.friction > 0 && bodyB->material.friction > 0)
        mu = (bodyA->material.friction + bodyB->material.friction) / 2;
      else
        continue;

      // clamp the friction
      Vec2 tangentImpulse;
      if (abs(tangentMagnitude) < magnitude * mu)
      {
        tangentImpulse = tangent * tangentMagnitude;
      }
      else
      {
        tangentImpulse = tangent * -magnitude * mu;
      }

      // add friction impulse
      bodyA->velocity -= tangentImpulse * AInverseMass;
      bodyB->velocity += tangentImpulse * BInverseMass;

      // add friction rotation
      added = AInverseInertia * bodyAToContact.Cross(tangentImpulse);
      bodyA->angularVelocity -= added;

      added = BInverseInertia * bodyBToContact.Cross(tangentImpulse);
      bodyB->angularVelocity += added;
    }
  }


  void Manifold::PositionalCorrection()
  {
    // set variables for the two bodies' mass (accounting for fixed)
    float AInverseMass = bodyA->GetFixed() ? 0 : bodyA->massData.inverseMass;
    float BInverseMass = bodyB->GetFixed() ? 0 : bodyB->massData.inverseMass;

    //// positional correction
    Vec2 correction = collisionNormal * (CORRECTION * collisionDepth / (AInverseMass + BInverseMass));
    bodyA->GetOwner()->transform.AddTranslation(correction * -1 * AInverseMass);
    bodyB->GetOwner()->transform.AddTranslation(correction * BInverseMass);

  }
}
