/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/
#include "RigidBody.h"
#include "Shape.h"
#include <vector>
#include "Manifold.h"
#include "PhysicsObject.h"
#include "Scene.h"

namespace Solid
{
  extern Scene scene;

  const float RigidBody::physicsFrameRate = 60;
  const float RigidBody::physicsTimestep = 1 / RigidBody::physicsFrameRate;
  RigidBody::CollisionState RigidBody::collisionTable[8][8] = { CollisionState(0) };
  float RigidBody::accumulator = 0;
  float RigidBody::gravity = 5.981f;
  std::vector<Manifold> RigidBody::manifolds;
  unsigned RigidBody::physicsIterations = 50;
  AABBTree RigidBody::aabbTree;

  // default constructor
  RigidBody::RigidBody() : fixed(false), rotationLocked(false), ghost(false),
    totalImpulse(), totalForce(), velocity(), torque(0), angularVelocity(0), gravityScale(1), shape(nullptr),
    material(), group(Group::Default)
  {}

  // constructor that sets shape and material
  RigidBody::RigidBody(const Shape *shape_, const Material &material_) : fixed(false),
    rotationLocked(false), ghost(false), totalImpulse(), totalForce(), velocity(), angularVelocity(0),
    gravityScale(1), torque(0), group(Group::Default)
  {
    // copy the material data over
    material = material_;

    // get a new copy of the shape
    shape = shape_->Clone();

    // make the shape point at this rigid body
    shape->body = this;

    // initialize the shape. this will also compute mass data
    shape->Initialize();
  }

  // copy constructor
  RigidBody::RigidBody(const RigidBody &rhs) : fixed(rhs.fixed), rotationLocked(rhs.rotationLocked), ghost(rhs.ghost),
    totalImpulse(rhs.totalImpulse), totalForce(rhs.totalForce), velocity(rhs.velocity), torque(rhs.torque),
    angularVelocity(rhs.angularVelocity), gravityScale(rhs.gravityScale), material(rhs.material),
    group(rhs.group), massData(rhs.massData)
  {
    shape = rhs.shape->Clone();
  }

  RigidBody::~RigidBody()
  {
    // make sure to delete the cloned shape
    delete shape;

    // make sure to tell the AABB tree
    aabbTree.RemoveLeaf(nodeHandle.m_index);
  }

  ObjectHandle RigidBody::GetOwner() const
  {
    return owner;
  }

  void RigidBody::SetOwner(PhysicsObject *owner_)
  {
    owner.object = owner_;
  }

  // TODO_DERHUI make this not suck
  AABB RigidBody::GetBoundingBox()
  {
    // get minX, minY, maxX, maxY
    float minX, minY, maxX, maxY;

    // get bounding box of a polygon
    if (shape->type == Shape::Type::Polygon)
    {
      Polygon *poly = reinterpret_cast<Polygon *>(shape);

      // get transformed indices
      Matrix3 transform = owner->transform.GetMatrix3();
      
      // set the first point to max and min
      Vec2 firstPoint = transform * poly->vertices[0];

      minX = maxX = firstPoint.x;
      minY = maxY = firstPoint.y;

      // now loop through all other points
      int vertexCount = poly->vertexCount;
      for (int i = 1; i < vertexCount; ++i)
      {
        Vec2 point = transform * poly->vertices[i];

        if (point.x > maxX)
          maxX = point.x;
        if (point.x < minX)
          minX = point.x;

        if (point.y > maxY)
          maxY = point.y;
        if (point.y < minY)
          minY = point.y;
      }

    }

    // if circle, logic is easier
    else if (shape->type == Shape::Type::Circle)
    {
      Circle *circle = reinterpret_cast<Circle *>(shape);

      float scale = owner->transform.GetScale().x;
      Vec2 position = owner->transform.GetTranslation();
      float radius = circle->radius * scale;

      minX = position.x - radius;
      maxX = position.x + radius;

      minY = position.y - radius;
      maxY = position.y + radius;

    }

    // now that we have all the necessary values, create bounding box
    AABB boundingBox;
    boundingBox.m_min = Vec2(minX, minY);
    boundingBox.m_max = Vec2(maxX, maxY);

    return boundingBox;
  }

  // TODO_DERHUI make sure to check for changes before calculating everything again
  std::vector<Vec2> RigidBody::GetWorldVertices()
  {
    // if circle, return empty vector
    if (shape->type == Shape::Type::Circle)
      return std::vector<Vec2>();

    // transform all verts of current object to world coords
    Polygon *poly = reinterpret_cast<Polygon *>(shape);
    std::vector<Vec2> worldVerts(poly->vertices, poly->vertices + poly->vertexCount);

    // get the matrix from the transform and multiply by world to camera
    Matrix3 transform = owner->transform.GetMatrix3();

    // apply transformation to all verts
    for (auto it = worldVerts.begin(); it != worldVerts.end(); ++it)
    {
      transform.Apply(*it);
    }

    return worldVerts;
  }

  void RigidBody::Set(const Shape *shape_, const Material &material_)
  {
    std::string nameA = GetOwner()->name;

    // clear the old shape, if there is one
    if (shape)
      delete shape;

    // copy the material data over
    material = material_;

    // get a new copy of the shape
    shape = shape_->Clone();

    // make the shape point at this rigid body
    shape->body = this;

    // initialize the shape. this will also compute mass data
    shape->Initialize();
  }

  void RigidBody::Set(const Material &material_)
  {
    try
    {
      // copy the material data over
      material = material_;

      // initialize the shape. this will also compute mass data
      if (shape)
      {
        shape->Initialize();
      }
      // if no shape, throw exception
      else
      {
        throw std::exception();
      }
    }

    // catches exception if no shape
    catch (std::exception e)
    {
      // TODO
    }
  }

  // gettor/settor for gravity scale
  float RigidBody::GetGravityScale() const
  {
    return gravityScale;
  }

  void RigidBody::SetGravityScale(float newScale)
  {
    gravityScale = newScale;
  }

  // gettor for shape
  Shape *RigidBody::GetShape() const
  {
    return shape;
  }

  // set switches
  void RigidBody::SetGhost(bool ghost_)
  {
    ghost = ghost_;
  }

  void RigidBody::SetFixed(bool fixed_)
  {
    fixed = fixed_;
  }

  void RigidBody::SetRotationLocked(bool locked)
  {
    rotationLocked = locked;
  }

  // toggle switches
  void RigidBody::ToggleGhost()
  {
    ghost = ghost ? false : true;
  }
  void RigidBody::ToggleFixed()
  {
    fixed = fixed ? false : true;
  }
  void RigidBody::ToggleRotationLocked()
  {
    rotationLocked = rotationLocked ? false : true;
  }

  RigidBody::CollisionState RigidBody::GetCollisionTableState(Group group1, Group group2)
  {
    return collisionTable[int(group1)][int(group2)];
  }

  void RigidBody::SetCollisionTableState(Group group1, Group group2, CollisionState state)
  {
    collisionTable[int(group1)][int(group2)] = state;
  }

  // collision mask functions
  void RigidBody::SetGroup(Group group_)
  {
    group = group_;
  }

  RigidBody::Group RigidBody::GetGroup() const
  {
    return group;
  }

  // integrates linear and angular velocities
  void RigidBody::Integrate(float dt)
  {
    // loop through all physics objects
    std::vector<PhysicsObject> &objects = scene.GetObjects();
    size_t numObjects = objects.size();
    for (size_t i = 0; i < numObjects; ++i)
    {
      // skip if inactive
      if (!objects[i].active)
        continue;

      // set up pointer to the current rigid body
      RigidBody *bodyA = objects[i].body;

      Transform &trans = bodyA->owner->transform;

      // store previous transform state
      trans.lastScale = trans.GetScale();
      trans.lastTranslation = trans.GetTranslation();
      trans.lastRotation = trans.GetRotation();

      // add forces to impulse
      Vec2 forcesImpulse = bodyA->totalForce * dt;
      bodyA->totalImpulse += forcesImpulse;

      // skip if object is static
      if (bodyA->fixed)
        continue;

      bodyA->velocity.y -= gravity * bodyA->gravityScale * dt;

      // add linear velocity
      bodyA->velocity += bodyA->totalImpulse;
      bodyA->owner->transform.AddTranslation(bodyA->velocity * dt);

      // add torque
      bodyA->angularVelocity += bodyA->torque;

      // add rotational velocity
      bodyA->owner->transform.AddRotation(bodyA->angularVelocity * dt);

      bodyA->totalForce.y = 0;
      bodyA->totalForce.x = 0;
      bodyA->totalImpulse.x = 0;
      bodyA->totalImpulse.y = 0;
      bodyA->torque = 0;
    }
  }

  void RigidBody::ResolveCollisions(float dt)
  {
    // clear the vector of manifolds
    manifolds.clear();

    // tell the AABBtree to get collision
    aabbTree.GetCollisions();

    CollisionPairs &pairs = aabbTree.m_collisionPairs;

    auto end = pairs.end();
    for (auto it = pairs.begin(); it != end; ++it)
    {
      // set up pointer to the current rigid body
      RigidBody *bodyA = it->bodyA;
      RigidBody *bodyB = it->bodyB;  
      
      // skip if both objects are static
      if (bodyA->fixed && bodyB->fixed)
        continue;
      // skip if collision table says to not resolve
      if (RigidBody::GetCollisionTableState(bodyA->GetGroup(), bodyB->GetGroup()) == CollisionState::Ignore)
        continue;

      // do collision and get manifold data
      Manifold result;
      bodyA->ResolveCollision(result, *bodyB);

      // if no collision, skip to next object
      if (result.collision == false)
        continue;

      // push the manifold into the vector of collisions
      if (result.flip)
      {
        result.bodyA = bodyB;
        result.bodyB = bodyA;
      }
      else
      {
        result.bodyB = bodyB;
        result.bodyA = bodyA;
      }

      manifolds.push_back(result);
    }

    /*  START code that doesn't use AABB tree

    // loop through all physics objects
    std::vector<PhysicsObject> &objects = scene.GetObjects();
    size_t numObjects = objects.size();
    for (size_t i = 0; i < numObjects; ++i)
    {
      // set up pointer to the current rigid body
      RigidBody *bodyA = objects[i].body;

      // for each object, loop through every other object
      for (unsigned j = i + 1; j < numObjects; ++j)
      {
        // set up pointer to the other rigid body
        RigidBody *bodyB = objects[j].body;

        // skip if both objects are static
        if (bodyA->fixed && bodyB->fixed)
          continue;

        // skip if collision table says to not resolve
        if (RigidBody::GetCollisionTableState(bodyA->GetGroup(), bodyB->GetGroup()) == CollisionState::Ignore)
          continue;

        // do collision and get manifold data
        Manifold result;
        bodyA->ResolveCollision(result, *bodyB);

        // if no collision, skip to next object
        if (result.collision == false)
          continue;

        // push the manifold into the vector of collisions
        if (result.flip)
        {
          result.bodyA = bodyB;
          result.bodyB = bodyA;
        }
        else
        {
          result.bodyB = bodyB;
          result.bodyA = bodyA;
        }

        manifolds.push_back(result);
      }
    }

    END code that doesn't use AABB tree*/

    // once all manifolds are generated, resolve the collisions in them
    // this process is repeated to create better accuracy
    size_t numManifolds = manifolds.size();
    for (unsigned i = 0; i < physicsIterations; ++i)
    {
      for (size_t j = 0; j < numManifolds; ++j)
      {
        manifolds[j].ApplyCollisionImpulse(dt);
      }
    }

    // now do positional correction, once all impulses are done
    for (unsigned i = 0; i < numManifolds; ++i)
    {
      Manifold current = manifolds[i];
      current.PositionalCorrection();
    }
  }
}
