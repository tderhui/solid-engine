/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#include "AABBTree.h"
#include "RigidBody.h"
#include "glut.h"

namespace Solid
{
  AABBNode AABBTree::s_invalidNode(-1);

  AABBTree::AABBTree() : m_rootIndex(-1), m_nodes()
  {
    // set the AABBNode's data pointer
    AABBNode::s_data = m_nodes.data();

    // set the vector handle's data pointer
    VectorHandle<AABBNode>::s_data = m_nodes.data();
  }

  AABBTree::~AABBTree() {}

  void AABBTree::Clear()
  {
    m_nodes.clear();
    m_rootIndex = -1;

    while (!m_freeList.empty())
      m_freeList.pop();
  }

  int AABBTree::GetTreeHeight(AABBNode &tree)
  {
    if (tree.IsLeaf())
    {
      return 0;
    }

    // if it is a branch, get the height of the children
    else
    {
      int leftHeight = GetTreeHeight(tree.GetLeftChild());
      int rightHeight = GetTreeHeight(tree.GetRightChild());

      int result = leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
      return result;
    }
  }

  ObjectHandle AABBTree::QueryPoint(Vec2 point)
  {
    if (m_rootIndex == -1)
      return ObjectHandle();

    AABBNode root = GetRoot();

    return QueryPointHelper(root, point);
  }

  ObjectHandle AABBTree::QueryPointHelper(AABBNode &tree, Vec2 point)
  {
    // first, check AABB
    if (!tree.GetBoundingBox().Contains(point))
      return ObjectHandle();

    // if leaf, just check the thing
    if (tree.IsLeaf())
    {
      if (tree.GetRigidBody()->Contains(point))
        return tree.GetRigidBody()->GetOwner();
    }

    // if branch, check children
    else
    {
      ObjectHandle result = QueryPointHelper(tree.GetLeftChild(), point);

      if (result.IsValid())
        return result;

      return QueryPointHelper(tree.GetRightChild(), point);
    }

    return ObjectHandle();
  }

  void AABBTree::Update()
  {
    // if tree is empty, get out
    if (m_rootIndex == -1)
      return;

    AABBNode &root = m_nodes[m_rootIndex];

    // first, clear the invalid nodes
    m_invalidNodeIndices.clear();

    // now get the invalid nodes
    FindInvalidNodes(root);

    // reinsert all invalid nodes
    auto end = m_invalidNodeIndices.end();
    for (auto it = m_invalidNodeIndices.begin(); it != end; ++it)
    {
      int workingIndex = *it;

      // replace the parent with the sibling
      ChildParentSet set = RemoveLeafHelper(workingIndex);

      // Recalculate bounding boxes on the nodes affected by removal
      if (set.m_newBranch != -1)
        m_nodes[set.m_newBranch].CalculateBoundingBox();

      // delete the parent
      if (set.m_parent != -1)
        m_freeList.push(set.m_parent);

      // if tree is empty, make it the root
      if (m_rootIndex == -1)
      {
        m_rootIndex = workingIndex;

        GetRoot().CalculateBoundingBox();
      }

      // else just reinsert the node
      else
      {
        AABBNode &insertedNode = InsertHelper(GetRoot(), m_nodes[workingIndex]);

        // now recalculate AABBs
        insertedNode.CalculateBoundingBox();
      }
    }
  }

  int AABBTree::Insert(AABBNode &node)
  {
    // if the node has already been inserted, just return the index of that node
    if (node.m_inserted)
      return node.GetIndex();

    int capacity = m_nodes.capacity();
    int index = -1;

    // check if there are any free slots. if there are, use them
    if (!m_freeList.empty())
    {
      index = m_freeList.top();
      m_freeList.pop();

      m_nodes[index] = node;
    }

    // if no empty spots, just use the back
    else
    {
      index = m_nodes.size();

      m_nodes.push_back(node);
    }

    m_nodes[index].SetIndex(index);
    m_nodes[index].m_inserted = true;

    // if the vector has grown, remember to update the AABBNode's s_data pointer
    if (m_nodes.capacity() != capacity)
    {
      AABBNode::s_data = m_nodes.data();

      // set the vector handle's data pointer
      VectorHandle<AABBNode>::s_data = m_nodes.data();
    }
    return index;
  }

  AABBNode &AABBTree::InsertHelper(AABBNode &tree, AABBNode &newNode)
  {
    // if i've reached a leaf, time to insert
    if (tree.IsLeaf())
    {
      // if inserting to root, root becomes leaf, new root is a branch node
      if (tree.GetParentIndex() == -1)
      {
        AABBNode newBranch;

        // get required indices, in case the vector grows
        int index = tree.GetIndex();

        // these two functions might cause the vector to grow
        int newBranchIndex = Insert(newBranch);
        int newNodeIndex = Insert(newNode);

        // link branch to two leaves
        m_nodes[newBranchIndex].SetLeftIndex(index);
        m_nodes[newBranchIndex].SetRightIndex(newNodeIndex);

        m_nodes[index].SetParentIndex(newBranchIndex);
        m_nodes[newNodeIndex].SetParentIndex(newBranchIndex);

        // recalculate bounding volume. recursively updates up tree
        m_nodes[newBranchIndex].CalculateBoundingBox();

        // update the root
        m_rootIndex = newBranchIndex;

        // now return it
        return m_nodes[newNodeIndex];

      }

      // if no sibling, make the new node the sibling
      else if (tree.GetSiblingIndex() == -1)
      {
        // get required indices, in case the vector grows
        int parentIndex = tree.GetParentIndex();

        // this actually puts it into the vector
        int newNodeIndex = Insert(newNode);

        // now link up the parent
        m_nodes[parentIndex].SetChild(newNodeIndex);
        m_nodes[newNodeIndex].SetParentIndex(parentIndex);

        // recalculate bounding boxes
        m_nodes[parentIndex].CalculateBoundingBox();

        // now return it
        return m_nodes[newNodeIndex];
      }

      // if it does have a sibling, make current node a branch node, with two children
      else
      {
        AABBNode newBranch;

        // get required indices, in case the vector grows
        int parentIndex = tree.GetParentIndex();
        int index = tree.GetIndex();

        // these two functions might cause the vector to grow
        int newNodeIndex = Insert(newNode);
        int newBranchIndex = Insert(newBranch);

        // now link parent to the branch
        m_nodes[parentIndex].ReplaceChild(index, newBranchIndex);
        m_nodes[newBranchIndex].SetParentIndex(parentIndex);

        // then link branch to two leaves
        m_nodes[newBranchIndex].SetLeftIndex(index);
        m_nodes[newBranchIndex].SetRightIndex(newNodeIndex);

        m_nodes[index].SetParentIndex(newBranchIndex);
        m_nodes[newNodeIndex].SetParentIndex(newBranchIndex);

        // recalculate bounding volume. recursively updates up tree
        m_nodes[newBranchIndex].CalculateBoundingBox();

        // now return it
        return m_nodes[newNodeIndex];
      }

    }

    // else, calculate least volume increase and recursively call functions
    AABBNode &leftChild = tree.GetLeftChild();
    AABBNode &rightChild = tree.GetRightChild();

    AABB left = leftChild.GetBoundingBox() + newNode.GetBoundingBox();
    AABB right = rightChild.GetBoundingBox() + newNode.GetBoundingBox();

    if (left.GetVolume() <= right.GetVolume())
      return InsertHelper(leftChild, newNode);

    return InsertHelper(rightChild, newNode);
  }

  void AABBTree::ClearComparedFlags()
  {
    auto end = m_nodes.end();
    for (auto it = m_nodes.begin(); it != end; ++it)
      it->m_childrenCompared = false;
  }

  void AABBTree::AddBodyPair(RigidBody *bodyA, RigidBody *bodyB)
  {
    CollisionPair pair = { bodyA, bodyB };

    m_collisionPairs.push_back(pair);
  }

  // it actually cut down the number of pairs from 136 to 39!
  void AABBTree::GetCollisions()
  {
    // first things first, clear the collisions
    m_collisionPairs.clear();

    // if tree is empty, don't do anything
    if (m_rootIndex == -1)
      return;

    // if only one object in the tree, don't compare
    if (GetRoot().IsLeaf())
      return;

    // reset performance info
    m_numDuplicates = 0;

    // reset the compared flags
    ClearComparedFlags();

    // AddAllBodiesToCollisionList(GetRoot());
    // m_collisionPairs.clear();

    GetCollisionsHelper(GetRoot().GetLeftChild(), GetRoot().GetRightChild());
  }

  void AABBTree::CompareChildren(AABBNode &tree)
  {
    if (!tree.m_childrenCompared)
    {
      GetCollisionsHelper(tree.GetRightChild(), tree.GetLeftChild());
      tree.m_childrenCompared = true;
    }
  }

  // this function will check if AABBs are colliding before 
  // evaluating if they need to be added to collision pairs
  void AABBTree::GetCollisionsHelper(AABBNode &node1, AABBNode &node2)
  {
    // compares the children before checking bounding box intersection
    if (!node1.IsLeaf())
      CompareChildren(node1);
    if (!node2.IsLeaf())
      CompareChildren(node2);

    // if bounding boxes do not intersect, do not cross check the children
    if (!node1.GetBoundingBox().Intersects(node2.GetBoundingBox()))
      return;

    // if it is a leaf, start adding all the other pairs
    if (node1.IsLeaf())
    {
      // 2 leaves, just push to result
      if (node2.IsLeaf())
      {
        AddBodyPair(node1.GetRigidBody(), node2.GetRigidBody());
      }

      // 1 leaf, 1 branch, do a cross check
      else
      {
        GetCollisionsHelper(node1, node2.GetLeftChild());
        GetCollisionsHelper(node1, node2.GetRightChild());
      }

    }

    // same thing as above, but node 2 is leaf and node 1 is branch instead
    else if (node2.IsLeaf())
    {
      GetCollisionsHelper(node2, node1.GetLeftChild());
      GetCollisionsHelper(node2, node1.GetRightChild());
    }

    // two branches, do all the cross checks
    else
    {
      GetCollisionsHelper(node1.GetLeftChild(), node2.GetLeftChild());
      GetCollisionsHelper(node1.GetLeftChild(), node2.GetRightChild());
      
      GetCollisionsHelper(node1.GetRightChild(), node2.GetLeftChild());
      GetCollisionsHelper(node1.GetRightChild(), node2.GetRightChild());
    }
  }

  // mainly used for debugging
  void AABBTree::AddAllBodiesToCollisionList(AABBNode &tree)
  {
    AddAllBodiesHelper(tree.GetLeftChild(), tree.GetRightChild());
  }

  // mainly used for debugging
  void AABBTree::AddAllBodiesHelper(AABBNode &node1, AABBNode &node2)
  {
    // if it is a leaf, start adding all the other pairs
    if (node1.IsLeaf())
    {
      // 2 leaves, just push to result
      if (node2.IsLeaf())
      {
        AddBodyPair(node1.GetRigidBody(), node2.GetRigidBody());
      }

      // 1 leaf, 1 branch, do a cross check
      else
      {
        AddAllBodiesHelper(node2.GetRightChild(), node2.GetLeftChild());
        AddAllBodiesHelper(node1, node2.GetLeftChild());
        AddAllBodiesHelper(node1, node2.GetRightChild());
      }

    }

    // same thing as above, but node 2 is leaf and node 1 is branch instead
    else if (node2.IsLeaf())
    {
      AddAllBodiesHelper(node1.GetRightChild(), node1.GetLeftChild());
      AddAllBodiesHelper(node2, node1.GetLeftChild());
      AddAllBodiesHelper(node2, node1.GetRightChild());
    }

    // two branches, do all the cross checks
    else
    {
      AddAllBodiesHelper(node1.GetRightChild(), node1.GetLeftChild());
      AddAllBodiesHelper(node1.GetLeftChild(), node2.GetLeftChild());
      AddAllBodiesHelper(node1.GetLeftChild(), node2.GetRightChild());

      AddAllBodiesHelper(node2.GetRightChild(), node2.GetLeftChild());
      AddAllBodiesHelper(node1.GetRightChild(), node2.GetLeftChild());
      AddAllBodiesHelper(node1.GetRightChild(), node2.GetRightChild());
    }

  }
  

  AABBNode &AABBTree::AddObject(RigidBody *object)
  {
    AABBNode newNode;
    newNode.SetRigidBody(object);

    // if tree is empty, just add to vector
    if (m_rootIndex == -1)
    {
      int rootIndex = Insert(newNode);

      m_rootIndex = rootIndex;
      return m_nodes[m_rootIndex];
    }

    // recursive function to add the new node into the vector
    return InsertHelper(m_nodes[m_rootIndex], newNode);
  }

  // for this AABBTree, you're really only going to be removing leaf nodes
  void AABBTree::RemoveLeaf(int index)
  {
    ChildParentSet set = RemoveLeafHelper(index);

    // if there's still stuff in there, update the bounding box
    if (set.m_newBranch != -1)
      m_nodes[set.m_newBranch].CalculateBoundingBox();

    if (set.m_child != -1)
      m_freeList.push(set.m_child);

    if (set.m_parent != -1)
      m_freeList.push(set.m_parent);
  }

  AABBNode &AABBTree::GetRoot()
  {
    return m_nodes[m_rootIndex];
  }

  int AABBTree::GetRootIndex()
  {
    return m_rootIndex;
  }
 
  std::vector<AABB> AABBTree::GetAllAABBs()
  {
    std::vector<AABB> result;

    // if empty, don't query
    if (m_rootIndex == -1)
      return result;

    GetAllAABBsHelper(GetRoot(), result);

    return result;
  }

  void AABBTree::GetAllAABBsHelper(AABBNode &tree, std::vector<AABB> &result)
  {
    result.push_back(tree.GetBoundingBox());

    if (tree.GetLeftIndex() != -1)
      GetAllAABBsHelper(tree.GetLeftChild(), result);
    if (tree.GetRightIndex() != -1)
      GetAllAABBsHelper(tree.GetRightChild(), result);
  }

  ChildParentSet AABBTree::RemoveLeafHelper(int index)
  {
    ChildParentSet ret;

    AABBNode &nodeToDelete = m_nodes[index];

    // if not root, replace parent with sibling
    if (nodeToDelete.GetParentIndex() != -1)
    {
      int siblingIndex = nodeToDelete.GetSiblingIndex();
      int parentIndex = nodeToDelete.GetParentIndex();

      AABBNode &parent = m_nodes[parentIndex];
      int grandparentIndex = parent.GetParentIndex();

      // if grandparent exists, update links
      if (grandparentIndex != -1)
      {
        AABBNode &grandparent = m_nodes[grandparentIndex];

        grandparent.ReplaceChild(parentIndex, siblingIndex);
      }

      // if parent is root, make sibling the root
      else
      {
        m_rootIndex = siblingIndex;
        m_nodes[siblingIndex].SetParentIndex(-1);
      }

      // now set the necessary information
      ret.m_child = index;
      ret.m_parent = parentIndex;
      ret.m_newBranch = siblingIndex;
    }

    // if deleting root
    else
    {
      m_rootIndex = -1;
      ret.m_child = index;
    }

    return ret;
  }

  void AABBTree::FindInvalidNodes(AABBNode &node)
  {
    // if it is a branch, just recursively call this function on the children
    if (!node.IsLeaf())
    {
      if (node.GetLeftIndex() != -1)
        FindInvalidNodes(node.GetLeftChild());
      if (node.GetRightIndex() != -1)
        FindInvalidNodes(node.GetRightChild());
    }

    // if it is a leaf, check the bounding box stuff
    else
    {
      AABB fatAABB = node.GetBoundingBox();

      // if the rigidbody's AABB no longer fits in the fatAABB, mark it as invalid
      if (!fatAABB.Contains(node.GetRigidBody()->GetBoundingBox()))
        m_invalidNodeIndices.push_back(node.GetIndex());
    }
  }

}