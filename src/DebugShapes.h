/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once

#include "Vec2.h"

namespace Solid
{
  struct Point
  {
    static void Draw(Vec2 translation);
  };

  struct Arrow
  {
    static void Draw(Vec2 translation, Vec2 direction);
  };

}