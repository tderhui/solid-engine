/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once

#include "RigidBody.h"
#include "Matrix3.h"

static const float PI = 3.141593f;
const unsigned int MaxVertexCount = 32;

namespace Solid{
  struct Shape
  {
    enum class Type {
      Polygon,
      Circle
    };

    Shape();
    virtual Shape *Clone() const = 0;
    virtual void Initialize() = 0;
    virtual void ComputeMass() = 0;
    virtual void SetOrientation(float radians) = 0;
    virtual Type GetType() const = 0;
    Vec2 center;

    RigidBody *body;

    Type type;
  };

  struct Circle : public Shape
  {
    Circle(float radius_);

    Shape *Clone() const;
    void Initialize();
    void ComputeMass();
    void SetOrientation(float radians);
    Type GetType() const;

    float radius;
  };

  struct Polygon : public Shape
  {
    Polygon();
    Polygon(float width, float height);
    Polygon(Vec2 vertices_[], unsigned int vertexCount_);

    Shape *Clone() const;
    void Initialize();
    void ComputeMass();
    void SetOrientation(float radians);
    Type GetType() const;

    void SetBox(float width, float height);
    void Set(Vec2 vertices_[], unsigned int vertexCount_);

    unsigned int vertexCount;
    Vec2 vertices[MaxVertexCount];
    Vec2 normals[MaxVertexCount];
    Matrix3 rotationMatrix;
  };
}