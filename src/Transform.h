/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once

#include "Matrix3.h"

namespace Solid
{

  class Transform
  {
  public:
    Matrix3 transformMatrix;

    Vec2 GetTranslation();
    Vec2 GetScale();
    float GetRotation();

    Matrix3 GetMatrix3();
    Matrix3 GetRenderMatrix3();

    void SetTranslation(Vec2 newTrans);
    void SetScale(Vec2 newScale);
    void SetRotation(float rads);

    void AddTranslation(Vec2 newTrans);
    void AddRotation(float rads);

    Transform();
    ~Transform();

    Vec2 lastTranslation;
    Vec2 lastScale;
    float lastRotation;

  private:
    bool hasChanged;
    Vec2 translation;
    Vec2 scale;
    float rotation;
  };

}