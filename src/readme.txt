Solid Engine by Tai Der Hui

Written as a standalone physics demo, used to test out 2D physics.

Click anywhere on the screen to generate a random object.

Solution was made in Visual Studio 2013, and is required to compile this project.

Link to solution can be found at:

http://tdhgames.webs.com/

or

https://bitbucket.org/tderhui/solid-engine.git