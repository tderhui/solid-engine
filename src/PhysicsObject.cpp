/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#include "PhysicsObject.h"
#include "RigidBody.h"
#include "Shape.h"
#include "Scene.h"

namespace Solid
{
  // default constructor gets used to fill up vector
  PhysicsObject::PhysicsObject() : transform(), body(nullptr), active(false)
  {

  }

  PhysicsObject::PhysicsObject(float hw, float hh) : transform(), active(true)
  {
    // default to a box
    Polygon poly(hw * 2, hh * 2);
    body = new RigidBody();

    body->SetOwner(this);
    body->Set(&poly);
  }

  PhysicsObject::PhysicsObject(Shape *shape) : active(true)
  {
    body = new RigidBody();

    body->SetOwner(this);
    body->Set(shape);
  }

  // copy constructor
  PhysicsObject::PhysicsObject(const PhysicsObject &rhs) : name(rhs.name), transform(rhs.transform)
  {
    body = rhs.body;
    active = rhs.active;
  }

  PhysicsObject::~PhysicsObject()
  {
  }

  // beacuse vectors. ugh.
  void PhysicsObject::Destroy()
  {
    active = false;
    delete body;
    body = nullptr;
  }

  void PhysicsObject::UpdateHandle(PhysicsObject *newPointer)
  {
    // if inactive, don't do anything
    if (!active)
      return;
    
    // if the internal pointers in the handle are different, update them
    if (body->GetOwner().object != newPointer)
      body->SetOwner(newPointer);
  }
}