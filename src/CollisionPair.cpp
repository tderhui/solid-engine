/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#include "CollisionPair.h"

namespace Solid
{
  bool CollisionPair::operator==(const CollisionPair &rhs)
  {
    return (bodyA == rhs.bodyA && bodyB == rhs.bodyB) ||
      (bodyB == rhs.bodyA && bodyA == rhs.bodyB);
  }
}