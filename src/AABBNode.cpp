/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#include "AABBNode.h"
#include "RigidBody.h"

namespace Solid
{
  const float AABBNode::extraFat = 0.2f;

  AABBNode * AABBNode::s_data = nullptr;

  AABBNode *VectorHandle<AABBNode>::s_data = nullptr;

  // default constructor
  AABBNode::AABBNode(int index)
    : m_index(index), m_parent(-1), m_left(-1), m_right(-1),
    m_body(nullptr), m_inserted(false)
  {

  }

  AABBNode::AABBNode(int index, int parent, int left, int right)
    : m_index(index), m_parent(parent), m_left(left), m_right(right), 
    m_body(nullptr), m_inserted(false)
  {

  }

  bool AABBNode::IsLeaf()
  {
    // if no children, then it is a leaf
    if (m_left == -1 && m_right == -1)
      return true;

    return false;
  }

  bool AABBNode::IsValid()
  {
    // any node with an index of -1 is invalid
    return m_index != -1;
  }

  bool AABBNode::IsStillValid()
  {
    AABB actualAABB = m_body->GetBoundingBox();

    return m_boundingBox.Contains(actualAABB);
  }

  AABBNode &AABBNode::GetLeftChild()
  {
    if (m_left == -1)
      return AABBTree::s_invalidNode;

    return s_data[m_left];
  }

  AABBNode &AABBNode::GetRightChild()
  {
    if (m_right == -1)
      return AABBTree::s_invalidNode;

    return s_data[m_right];
  }

  AABBNode &AABBNode::GetParent()
  {
    if (m_parent == -1)
      return AABBTree::s_invalidNode;

    return s_data[m_parent];
  }

  AABB AABBNode::GetBoundingBox()
  {
    return m_boundingBox;
  }

  AABB AABBNode::GetBodyAABB()
  {
    if (IsLeaf())
    {
      return m_body->GetBoundingBox();
    }

    return AABB();
  }

  RigidBody *AABBNode::GetRigidBody()
  {
    if (IsLeaf())
    {
      return m_body;
    }
    return nullptr;
  }

  void AABBNode::SetRigidBody(RigidBody *body)
  {
    m_body = body;

    AABB bodyAABB = body->GetBoundingBox();
    Vec2 extraFatVector = { extraFat, extraFat };

    bodyAABB.m_min -= extraFatVector;
    bodyAABB.m_max += extraFatVector;

    m_boundingBox = bodyAABB;
    m_volume = m_boundingBox.GetVolume();
  }
  
  bool AABBNode::SetChild(int index)
  {
    if (m_left == -1)
    {
      m_left = index;
      return true;
    }
    else if (m_right == -1)
    {
      m_right = index;
      return true;
    }

    return false;
  }

  void AABBNode::CalculateBoundingBox()
  {
    // if is leaf, recalculate based on rigid body AABB
    if (IsLeaf())
    {
      AABB bodyAABB = m_body->GetBoundingBox();
      Vec2 extraFatVector = { extraFat, extraFat };

      bodyAABB.m_min -= extraFatVector;
      bodyAABB.m_max += extraFatVector;

      m_boundingBox = bodyAABB;
      m_volume = m_boundingBox.GetVolume();
    }

    // else just add up the left and right ones
    else
    {
      m_boundingBox = GetLeftChild().GetBoundingBox() + GetRightChild().GetBoundingBox();

      m_volume = m_boundingBox.GetVolume();
    }

    // now recalculate parents' bounding boxes
    if (GetParentIndex() != -1)
      GetParent().CalculateBoundingBox();
  }

  int AABBNode::GetIndex()
  {
    return m_index;
  }

  int AABBNode::GetSiblingIndex()
  {
    AABBNode &parent = GetParent();

    // if i am not the left child, return the left child
    if (parent.m_left != m_index)
      return parent.m_left;

    return parent.m_right;
  }

  int AABBNode::GetParentIndex()
  {
    return m_parent;
  }

  int AABBNode::GetLeftIndex()
  {
    return m_left;
  }

  int AABBNode::GetRightIndex()
  {
    return m_right;
  }

  void AABBNode::SetIndex(int index)
  {
    m_index = index;

    // also set the rigid body's nodeHandle index
    // boy am I glad I made m_index private
    if (m_body != nullptr)
      m_body->nodeHandle.m_index = index;
  }

  void AABBNode::SetParentIndex(int index)
  {
    m_parent = index;
  }

  void AABBNode::SetLeftIndex(int index)
  {
    m_left = index;
  }

  void AABBNode::SetRightIndex(int index)
  {
    m_right = index;
  }

  void AABBNode::RemoveChild(int index)
  {
    if (m_left == index)
      m_left = -1;
    else if (m_right == index)
      m_right = -1;

    s_data[index].m_parent = -1;
  }

  void AABBNode::ReplaceChild(int existing, int newChild)
  {
    if (m_left == existing)
      m_left = newChild;
    else if (m_right == existing)
      m_right = newChild;

    s_data[existing].m_parent = -1;
    s_data[newChild].m_parent = m_index;
  }
}