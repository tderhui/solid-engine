/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once

#include <string>
#include "Transform.h"

namespace Solid
{
  // forward declarations
  class RigidBody;
  class ObjectHandle;
  struct Shape;

  class PhysicsObject
  {
  public:
    PhysicsObject();
    PhysicsObject(float hw, float hh);
    PhysicsObject(const PhysicsObject &rhs);
    PhysicsObject(Shape *shape);
    ~PhysicsObject();

    void UpdateHandle(PhysicsObject *newPointer);
    void Destroy();

    bool operator==(const PhysicsObject &rhs)
    {
      return body == rhs.body;
    }

    std::string name;
    Transform transform;
    RigidBody *body;
    bool active;
  private:
  };

}