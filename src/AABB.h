/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once

#include "Vec2.h"

namespace Solid
{
  class RigidBody;

  class AABB
  {
  public:
    AABB(Vec2 min = Vec2(0, 0), Vec2 max = Vec2(0, 0));

    bool Contains(Vec2 point);
    bool Contains(const AABB &rhs);
    bool Contains(RigidBody *rhs);

    bool Intersects(const AABB &rhs);
    bool Intersects(RigidBody *rhs);

    AABB operator+(const AABB &rhs);

    float GetVolume();

    Vec2 m_min;
    Vec2 m_max;
  };

}