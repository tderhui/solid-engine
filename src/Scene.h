/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once

#include <ctime>
#include <vector>
#include "PhysicsObject.h"
#include "AABBTree.h"

namespace Solid
{
  static const unsigned numRandomShapes = 3;

  // used so that we can store objects in vectors
  class ObjectHandle
  {
  public:
    ObjectHandle();
    bool Update(PhysicsObject * newObject);

    bool IsValid()
    {
      return object != nullptr;
    }

    PhysicsObject *operator->() 
    {
      return object;
    }

    PhysicsObject *object;
  };

  class Scene
  {
  public:
    // constructor / destructor
    Scene::Scene();
    Scene::~Scene();

    // draws bounding boxes of all shapes
    void DrawBoundingBox();
    void DrawAABBTree();

    // generates a random polygon in the scene at position
    void GenerateRandomPolygonAtPosition(Vec2 translation);

    // generates a random circle in the scene at position
    void GenerateRandomCircleAtPosition(Vec2 translation);

    // draws arrow at the point
    void DrawArrowAtPoint(Vec2 point, Vec2 direction);

    // initialization
    void Initialize();

    // renders objects using glut
    void Render();

    // get the objects
    std::vector<PhysicsObject> &GetObjects();

    // push a new physics object into the scene
    void PushNewObject(PhysicsObject object);

    // removes an object from the scene
    void RemoveObject(ObjectHandle object);

    // removes unnecessary objects
    void CullOffScreenObjects();

    // clears all objects
    void ClearObjects();

    // frame rate controller stuff
    clock_t lastTime;
    clock_t currentTime;

    // bool to check if debug draw of bounding boxes should be turned on
    bool drawBoundingBox;

    // world to camera matrix
    Matrix3 worldToCamera;

    // flag to set whether to go frame by frame or not
    bool frameStepping;
    bool toStep;

  private:
    // array of random shape pointers
    Shape *shapes[numRandomShapes];

    // vector of physics objects
    std::vector<PhysicsObject> objects;

    // updates all handles in the objects
    void UpdateHandles();

  };
}