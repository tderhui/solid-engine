/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once

#include "AABB.h"

namespace Solid
{
  class RigidBody;

  class AABBNode
  {
  public:
    AABBNode(int index = -1); // node with index of -1 is considered empty/invalid
    AABBNode(int index, int parent, int left, int right);

    bool IsLeaf();
    bool IsValid();

    // checks if still fits within fat AABB
    // if it is a branch node, will return true
    bool IsStillValid();

    // only works on branches
    // invalid node will have index of -1
    AABBNode &GetLeftChild();
    AABBNode &GetRightChild();

    // does not check if it is root
    AABBNode &GetParent();

    // gets the fat AABB
    AABB GetBoundingBox();

    // only works on leaf nodes, gets the rigid body's AABB
    AABB GetBodyAABB();

    // returns nullptr if node is a branch
    RigidBody *GetRigidBody();
    
    // also sets the fat AABB
    void SetRigidBody(RigidBody *body);

    // sets child. favours left over right. returns false if no space for children
    bool SetChild(int index);

    // updates the fatAABB
    // also recursively updates up the tree
    void CalculateBoundingBox();

    // gettors/settors for indices
    int GetIndex();
    int GetSiblingIndex();
    int GetParentIndex();
    int GetLeftIndex();
    int GetRightIndex();

    void SetIndex(int index);
    void SetParentIndex(int index);
    void SetLeftIndex(int index);
    void SetRightIndex(int index);

    // sets the child index to -1
    void RemoveChild(int index);

    // replaces existing child with new child
    void ReplaceChild(int existing, int newChild);

    // static pointer to the array that holds all AABBNodes
    static AABBNode *s_data;

    // states whether this node has been inserted to the tree
    bool m_inserted;

    // flag to see if this node's children have been compared
    bool m_childrenCompared;

  private:
    // volume of the fatAABB
    float m_volume;

    // indices of its children and itself
    int m_parent;
    int m_index;
    int m_left;
    int m_right;

    AABB m_boundingBox;

    // actual RigidBody pointer
    RigidBody *m_body;

    // 'fat' around the aabb
    static const float extraFat;
  };

}