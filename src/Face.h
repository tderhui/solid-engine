/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once

#include "Vec2.h"

namespace Solid
{
  class Face
  {
  public:
    Vec2 point1;
    Vec2 point2;

    // default constructor
    Face() : point1(), point2() {}

    Face(Vec2 a, Vec2 b) : point1(a), point2(b) {}

    // returns the right normal
    Vec2 findNormal() const
    {
      Vec2 ret = { point2.y - point1.y, -(point2.x - point1.x) };
      return ret;
    }

    // finds the vector from point 1 to point 2
    Vec2 findVector() const
    {
      Vec2 ret = { point2.x - point1.x, point2.y - point1.y };
      return ret;
    }

    Vec2 findMidPoint() const
    {
      Vec2 ret = { point1.x + point2.x / 2, point1.y + point2.y / 2 };
      return ret;
    }

    Vec2 &operator[](const int index)
    {
      if (index == 0)
        return point1;
      else
        return point2;
    }
  };
}