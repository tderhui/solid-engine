/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#include "RigidBody.h"
#include "Shape.h"
#include "Manifold.h"
#include "Vec2.h"
#include <algorithm>
#include "DebugShapes.h"

namespace Solid
{
  const float RigidBody::s_EPATolerance = 0.0001f;

  RigidBody::PointIndexPair RigidBody::FindSupport(Vec2 direction)
  {
    std::vector<Vec2> myPoints = GetWorldVertices();

    int myLeastIndex = -1;
    int rhsLeastIndex = -1;
    float maxDotProduct = -1000000;

    // find max dot product of my points
    int numPoints = myPoints.size();
    for (int i = 0; i < numPoints; ++i)
    {
      float currDotProduct = myPoints[i] * direction;
      if (currDotProduct > maxDotProduct)
      {
        maxDotProduct = currDotProduct;
        myLeastIndex = i;
      }
    }

    PointIndexPair ret;
    ret.point = myPoints[myLeastIndex];
    ret.index = myLeastIndex;

    return ret;
  }

  RigidBody::SimplexPoint RigidBody::FindMinkowskiSupport(RigidBody &rhs, Vec2 direction)
  {
    PointIndexPair mySupport = FindSupport(direction);
    PointIndexPair rhsSupport = rhs.FindSupport(direction * -1);

    Vec2 supportPoint = mySupport.point - rhsSupport.point;
    SimplexPoint ret = { supportPoint, mySupport.index, rhsSupport.index };

    return ret;
  }

  bool RigidBody::DoSimplex(std::vector<SimplexPoint> &simplex, Vec2 &direction)
  {
    int numPoints = simplex.size();

    // line case
    if (numPoints == 2)
    {
      Vec2 &pointA = simplex[1].point;
      Vec2 &pointB = simplex[0].point;

      // get the direction vector from a to b
      Vec2 AB = pointB - pointA;

      // if the origin is in this direction, from B, then origin is within AB
      Vec2 originDirection = pointA * -1;
      if (AB * originDirection >= 0)
      {
        // get new direction
        Vec2 newDirection = { direction.y, -direction.x };
        if (newDirection * originDirection != 0)
          newDirection = { -direction.y, direction.x };

        direction = newDirection;
        return true;
      }

      // if the origin is NOT in the direction, then just return A
      // and direction AO
      simplex.pop_back();
      direction = pointA * -1;

    }

    // triangle case
    if (numPoints == 3)
    {
      Vec2 &pointA = simplex[2].point;
      Vec2 &pointB = simplex[1].point;
      Vec2 &pointC = simplex[0].point;

      // get line segments
      Vec2 AB = pointB - pointA;
      Vec2 AC = pointC - pointA;

      // find normals and check if they are valid
      Vec2 normAB = { -AB.y, AB.x };

      // first get center of triangle
      Vec2 triCenter = (pointA + pointB + pointC) / 3;

      // get vectors from center to A and B
      Vec2 centerToC = pointC - triCenter;
      Vec2 centerToB = pointB - triCenter;

      // the normal should be outward pointing, and thus should NOT point towards C
      if (normAB * centerToC > 0)
        normAB = { AB.y, -AB.x };

      // the normal should be outward pointing, and thus should NOT point towards B
      Vec2 normAC = { -AC.y, AC.x };
      if (normAC * centerToB > 0)
        normAB = { AC.y, -AC.x };

      // now that we have the normals, start checking
      Vec2 AO = pointA * -1;

      // if the origin is in line segment AB's half plane, do further checks
      if (normAB * AO > 0)
      {
        // if origin is in the direction AB, then we know origin is within line segment
        if (AB * AO > 0)
        {
          // move the points down to erase point C, to return AB
          simplex[0] = simplex[1];
          simplex[1] = simplex[2];
          simplex.pop_back();

          // return proper direction
          // TODO_DERHUI this is still wrong
          direction = normAB;

          return true;
        }

        // TODO_DERHUI not sure about this check
        // else, it is in a weird space 
        else
        {
          if (AC * AO > 0)
          {
            // move points down to erase point B, to return AC
            simplex[1] = simplex[2];
            simplex.pop_back();

            // return proper direction
            // TODO_DERHUI this is still wrong
            direction = normAC;

            return true;
          }

          else
          {
            // move point down and erase point B and C, to return A
            simplex[0] = simplex[2];
            simplex.pop_back();
            simplex.pop_back();

            // return direction
            direction = AO;

            return true;
          }
        }

      }

      // if the origin is NOT in line segment AB's half plane, do further checks
      else
      {
        // if origin is within line segment AC's half plane, we know where it is
        if (normAC * AO > 0)
        {
          if (AC * AO > 0)
          {
            // move points down to erase point B, to return AC
            simplex[1] = simplex[2];
            simplex.pop_back();

            // return proper direction
            // TODO_DERHUI this is still wrong
            direction = normAC;

            return true;
          }

          else
          {
            // move point down and erase point B and C, to return A
            simplex[0] = simplex[2];
            simplex.pop_back();
            simplex.pop_back();

            // return direction
            direction = AO;

            return true;
          }
        }

        // this is the terminating case
        else
        {
          return false;
        }
      }
    }

    // should not get here
    return false;
  }

  RigidBody::Edge RigidBody::EPA(std::vector<SimplexPoint> &simplex, Manifold &result, RigidBody &rhs)
  {
    // keep looping until we find the expanded polytope
    while (true)
    {
      // find the closest edge of the simplex to the origin
      Edge edge = FindClosestEdge(simplex);

      // get a new support point in the direction of the edge normal (outward pointing)
      SimplexPoint newSupportPoint = FindMinkowskiSupport(rhs, edge.normal);

      // if the distance from the new support point to origin is very similar
      // to the distance the current edge is from the origin, then the polytope
      // cannot be expanded any more, thus terminating the algorithm
      float newPointToOrigin = newSupportPoint.point * edge.normal;

      if (newPointToOrigin - edge.distance < s_EPATolerance)
      {
        return edge;
      }

      // if it can be expanded, then insert the new point at the correct position
      else
      {
        auto it = simplex.begin();
        simplex.insert(it + edge.index, newSupportPoint);
      }
    }

    return Edge(); // should not get here
  }

  // TODO_DERHUI needs to be optimized to not re-calculate the pre-existing edges
  RigidBody::Edge RigidBody::FindClosestEdge(std::vector<SimplexPoint> simplex)
  {
    int numPoints = simplex.size();
    Edge result;
    result.distance = 1000000000000;

    // for each edge, calculate min distance, and pick the least
    for (int i = 0; i < numPoints; ++i)
    {
      // get the next index
      int j = i + 1 == numPoints ? 0 : i + 1;
      int k = j + 1 == numPoints ? 0 : j + 1;

      SimplexPoint &A = simplex[i];
      SimplexPoint &B = simplex[j];

      // get the edge vector of these two points
      Vec2 edgeVector = B.point - A.point;

      // get the direction vector OA to find outward-pointing normal
      Vec2 OA = A.point;

      // weird notation for triple product to find outward-pointing normal
      Vec2 normal = Cross(edgeVector.Cross(OA), edgeVector);

      // normalize the normal so we can find distance to origin
      normal.Normalize();
      float distance = normal * A.point;

      // if this distance is the smallest, store it
      if (distance < result.distance)
      {
        result.distance = distance;
        result.normal = normal;
        result.index = j;
      }

    }

    return result;
  }

  bool RigidBody::TestGJKCollision(Manifold &result, RigidBody &rhs)
  {
    std::vector<SimplexPoint> simplex;
    std::vector<Vec2> myPoints = GetWorldVertices();
    std::vector<Vec2> rhsPoints = rhs.GetWorldVertices();

    // start with any random point and direction
    Vec2 firstPoint = myPoints[0] - rhsPoints[0];
    Vec2 direction = firstPoint * -1;
    simplex.push_back({ firstPoint, 0, 0 });

    // now loop
    while (true)
    {
      // find new minkowski support point in the given direction
      SimplexPoint newPoint = FindMinkowskiSupport(rhs, direction);

      // if new MSP does not reach the origin, it will never reach it..
      if (newPoint.point * direction < 0)
        return false;

      // if it is past the origin, then we keep searching
      simplex.push_back(newPoint);

      // if DoSimplex returns false, it means a collision has been found
      if (!DoSimplex(simplex, direction))
      {
        // let's do some EPA!
        Edge edge = EPA(simplex, result, rhs);
        result.collisionNormal = edge.normal;
        result.collisionDepth = edge.distance;
        result.contactCount = 1;
        result.contacts[0] = rhsPoints[simplex[edge.index].indexB];
        result.collision = true;
        
        
        return true;
      }
    }

    return false;
  }


}