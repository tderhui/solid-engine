/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/
#include <stdlib.h>
#include "glut.h"
#include "RigidBody.h"
#include "Scene.h"
#include "Clock.h"
#include "DebugShapes.h"

namespace Solid
{
  Clock g_clock;
  Scene scene;

  void Update()
  {
    // clear the screen
    glClearColor(0.925f, 0.925f, 0.925f, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // accumulate the dt
    if (!scene.frameStepping)
      RigidBody::accumulator += g_clock.Elapsed();

    else
    {
      if (scene.toStep)
      {
        RigidBody::accumulator += RigidBody::physicsTimestep;
        scene.toStep = false;
      }
    }

    // get frame time
    g_clock.Start();


    // loop through and keep updating until accumulator is small enough
    while (RigidBody::accumulator > RigidBody::physicsTimestep)
    {
      RigidBody::Integrate(RigidBody::physicsTimestep);
      RigidBody::ResolveCollisions(RigidBody::physicsTimestep);
      RigidBody::accumulator -= RigidBody::physicsTimestep;
      float accumulator = RigidBody::accumulator;

      // every time the rigid bodies move, the AABBtree needs to be updated
      RigidBody::aabbTree.Update();
    }

    // cull offscreen objects
    scene.CullOffScreenObjects();

    // now that physics is done updating, render
    scene.Render();
    
  }
  
  void Mouse(int button, int state, int x, int y)
  {
    float mouseX = x / 800.0f * 10 - 5;
    float mouseY = -y / 800.0f * 10 + 5;

    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
      scene.GenerateRandomPolygonAtPosition(Vec2(mouseX, mouseY));
    if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
      scene.GenerateRandomCircleAtPosition(Vec2(mouseX, mouseY));
    if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN)
    {
      ObjectHandle object = RigidBody::aabbTree.QueryPoint(Vec2(mouseX, mouseY));
      if (object.IsValid())
      {
        object->Destroy();
      }
    }
  }
  
  void glutKeyboardFunc(void(*func)(unsigned char key,
    int x, int y));

  void Keyboard(unsigned char key, int x, int y)
  {
    // p for pause!
    if (key == 'p')
      scene.frameStepping = !scene.frameStepping;

    // b for bounding box!
    if (key == 'b')
      scene.drawBoundingBox = !scene.drawBoundingBox;

    // n for next!
    if (key == 'n')
      scene.toStep = true;

    // r for restart!
    if (key == 'r')
    {
      scene.ClearObjects();
      scene.Initialize();
    }
  }

}


int main(int argc, char *argv[]) {
  Solid::scene.Initialize();
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutInitWindowPosition(0, 0);
  glutInitWindowSize(800, 800);
  glutCreateWindow("Solid Engine");
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glutDisplayFunc(Solid::Update);
  glutMouseFunc(Solid::Mouse);
  glutKeyboardFunc(Solid::Keyboard);
  glutIdleFunc(Solid::Update);
  glutMainLoop();
  return 0;
}