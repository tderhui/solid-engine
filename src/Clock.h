/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once

#include <chrono>
#include <Windows.h>

class Clock
{
public:
  Clock();
  ~Clock();

  // stores current tick count in start
  void Start();

  // stores current tick count in stop
  void Stop();

  // gets the amount of time passed since start
  float Elapsed();

  // gets the time diff between start and stop
  float Difference();

  // stores the current tick count in current
  LONGLONG Current();

private:
  LARGE_INTEGER freq;
  LARGE_INTEGER start, stop, current;

  void Query(LARGE_INTEGER& query);
};

