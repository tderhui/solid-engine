/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/
#pragma once

namespace Solid
{
  template <typename T>
  class VectorHandle
  {
  public:
    VectorHandle() : m_index(-1) {}

    T *const operator->() const { return &s_data[m_index]; }
    T *      operator->() { return &s_data[m_index]; }

    T const &operator*() const { return s_data[m_index]; }
    T &      operator*() { return s_data[m_index]; }
    
    VectorHandle<T> &operator= (int index) { m_index = index; return *this; }
    VectorHandle<T> &operator= (const VectorHandle<T> &rhs) { m_index = rhs.m_index; return *this; }
    
    bool operator==(const VectorHandle<T> &rhs) const { return rhs.m_index == m_index; }
    bool operator==(int index) const { return m_index == index; }

    bool operator!=(const VectorHandle<T> &rhs) const { return rhs.m_index != m_index; }
    bool operator!=(int index) const { return m_index != index; }
    
    operator bool() const { return isValid(); }
    
    bool     isValid() const { return (s_data != nullptr && index != -1); }

    static T *s_data;
    int m_index;
  };
}