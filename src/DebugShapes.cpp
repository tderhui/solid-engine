/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#include "DebugShapes.h"
#include "glut.h"

namespace Solid
{
  static const float PI = 3.14f;

  void Point::Draw(Vec2 translation)
  {
    glColor4f(0.1f, 0.1f, 0.8f, 1.0f);
    glBegin(GL_LINES);
    Vec2 transformedPoint = translation * 0.2f;
    float radius = 0.005f;

    // draw a circle for the point
    unsigned numIterations = 10;
    for (unsigned j = 0; j < numIterations; ++j)
    {
      float x = radius * cos(2 * PI * j / numIterations) + transformedPoint.x;
      float y = radius * sin(2 * PI * j / numIterations) + transformedPoint.y;
      glVertex3f(x, y, 0);

      x = radius * cos(2 * PI * (j + 1) / numIterations) + transformedPoint.x;
      y = radius * sin(2 * PI * (j + 1) / numIterations) + transformedPoint.y;
      glVertex3f(x, y, 0);
    }

    glEnd();
  }

  void Arrow::Draw(Vec2 translation, Vec2 direction)
  {
    glColor4f(0.8f, 0.1f, 0.1f, 1.0f);
    glBegin(GL_LINES);
    Vec2 transformedTranslation = translation * 0.2f;
    Vec2 transformedEndPoint = transformedTranslation + direction.Normalized() * 0.05f;

    // draw the arrow
    glVertex2f(transformedTranslation.x, transformedTranslation.y);
    glVertex2f(transformedEndPoint.x, transformedEndPoint.y);

    glEnd();
  }

}