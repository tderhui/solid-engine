/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once

#include "Manifold.h"
#include "Vec2.h"
#include "Matrix3.h"
#include "Face.h"
#include "Material.h"
#include "PhysicsObject.h"
#include "Scene.h"
#include "AABB.h"
#include "VectorHandle.h"

namespace Solid
{
  struct Shape; // forward declaration

  class RigidBody
  {
  public:
    enum class Group {
      Default = 0,
      Group1 = 1,
      Group2 = 2,
      Group3 = 3,
      Group4 = 4,
      Group5 = 5,
      Group6 = 6,
      Group7 = 7,
      Group8 = 8
    };

    enum class CollisionState {
      Resolve = 0,
      Ignore = 1
    };

    struct PointIndexPair
    {
      Vec2 point;
      int index;
    };

    // used to store information about a simplex to find contact points
    struct SimplexPoint
    {
      Vec2 point;
      int indexA;
      int indexB;
    };

    struct Edge
    {
      int index;
      Vec2 normal;
      float distance;
    };

    RigidBody();
    RigidBody(const RigidBody &rhs);
    RigidBody(const Shape *shape_, const Material &material_ = Material());
    ~RigidBody();

    /***************** GJK Stuff **********************/
    PointIndexPair FindSupport(Vec2 direction);
    SimplexPoint FindMinkowskiSupport(RigidBody &rhs, Vec2 direction);
    Edge RigidBody::EPA(std::vector<SimplexPoint> &simplex, Manifold &result, RigidBody &rhs);
    bool TestGJKCollision(Manifold &result, RigidBody &rhs);
    
    // returns true if algorithm needs to continue
    bool DoSimplex(std::vector<SimplexPoint> &simplex, Vec2 &direction);

    // static helper to find closest edge, given a simplex
    static Edge FindClosestEdge(std::vector<SimplexPoint> simplex);

    // tolerance for EPA to exit
    static const float s_EPATolerance;

    /**************** End GJK Stuff *******************/

    // function to set the shape and material of the rigid body
    void Set(const Shape *shape_, const Material &material_ = Material());
    void Set(const Material &material_);

    // gettor/settor for gravity scale
    float GetGravityScale() const;
    void SetGravityScale(float newScale);

    // query point
    bool Contains(Vec2 point);

    // gettor for shape
    Shape *GetShape() const;

    // get switches
    bool GetGhost() { return ghost; }
    bool GetFixed() { return fixed; }
    bool GetRotationLocked() { return rotationLocked; }

    // set switches
    void SetGhost(bool ghost_);
    void SetFixed(bool fixed_);
    void SetRotationLocked(bool locked);

    // toggle switches
    void ToggleGhost();
    void ToggleFixed();
    void ToggleRotationLocked();

    // collision group functions
    void SetGroup(Group group_);
    Group GetGroup() const;

    // function to get owner
    ObjectHandle GetOwner() const;
    void SetOwner(PhysicsObject *owner_);

    // bounding box stuff
    AABB GetBoundingBox();

    // get the transformed vertices of the rigid body
    // if the rigidbody is a circle, returns empty vector
    std::vector<Vec2> GetWorldVertices();

    // gravity
    static float gravity;

    // struct containing information about an object's mass
    struct {
      // linear mass data
      float mass;
      float inverseMass;

      // rotational mass data
      float inertia;
      float inverseInertia;
    } massData;

    // struct containing information about an object's material
    Material material;

    // object's current velocity
    Vec2 velocity;       
    
    // rotational velocity in radians
    float angularVelocity; 

    // linear components
    Vec2 totalImpulse;   // sum of all impulses at the end of a step
    Vec2 totalForce;     // forces that need to be multiplied by dt

    // rotational components
    float torque;       // sum of all additional rotational additions at the end of a step

    // material stuff
    std::string p_Material;
    std::string materialLast;

    // handle to its AABB node
    VectorHandle<AABBNode> nodeHandle;

    // collision mask
    Group group;

    // static functions
    static void RigidBody::ResolveCollisions(float dt);
    static void RigidBody::Integrate(float dt);

    // aabbtree for broadphase
    static AABBTree aabbTree;

    // number of iterations to carry out per frame
    static unsigned physicsIterations;

    // loops per frame for force application
    static float accumulator;

    // framerate stuff
    static const float physicsFrameRate;
    static const float physicsTimestep;

    // static 2D array for collision table
    static CollisionState collisionTable[8][8];

    // static function to get/set collision table state
    static CollisionState GetCollisionTableState(Group group1, Group group2);
    static void SetCollisionTableState(Group group1, Group group2, CollisionState state);

    // static vector of all manifolds of all collisions
    static std::vector<Manifold> manifolds;

  private:
    // collision functions
    void PolygonToPolygon(Manifold &result, RigidBody &rhs);
    void PolygonToCircle(Manifold &result, RigidBody &rhs);
    void CircleToPolygon(Manifold &result, RigidBody &rhs);
    void CircleToCircle(Manifold &result, RigidBody &rhs);

    // ref/incident face functions
    bool findReferenceFace(Face &referenceFace, Vec2 collisionNormal);
    bool findIncidentFace(Face &incidentFace, const Face &referenceFace, Vec2 collisionNormal);

    // clipping algorithm
    static unsigned Clip(Vec2 referenceNormal, float distOriginToFace, Face &inputFace, Face &contactPoints);
    
    // checks for collision with other rigid body and returns a manifold
    Manifold narrowCheck(RigidBody &rhs);

    // returns the penetration
    float findAxisOfLeastPenetration(const RigidBody &bodyB, Manifold &result) const;

    // calls the appropriate collision function based on shape type
    void ResolveCollision(Manifold &result, RigidBody &rhs);

    // switches
    bool fixed;           // switch to set whether object moves or not
    bool rotationLocked;  // switch to set whether object rotates or not
    bool ghost;           // switch to set whether object should collide or not

    // gravitational scale
    float gravityScale;

    // pointer to its shape
    Shape *shape;

    // handle to the physics object
    ObjectHandle owner;
  };
}