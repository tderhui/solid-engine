/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#include "Clock.h"

Clock::Clock()
{
  // assign to a single processor
  SetThreadAffinityMask(GetCurrentThread(), 1);

  // get the frequency of this processor
  QueryPerformanceFrequency(&freq);

  // set the initial times
  Start();
  Stop();
}

void Clock::Start()
{
  QueryPerformanceCounter(&start);
}

void Clock::Stop()
{
  QueryPerformanceCounter(&stop);
}

float Clock::Elapsed()
{
  QueryPerformanceCounter(&current);
  // take the number of ticks between started and current, and divide that by ticks per second
  return(current.QuadPart - start.QuadPart) / float(freq.QuadPart);
}

float Clock::Difference()
{
  // take the number of ticks between stop and started, and divide that by ticks per second
  return (stop.QuadPart - start.QuadPart) / float(freq.QuadPart);
}

LONGLONG Clock::Current()
{
  QueryPerformanceCounter(&current);
  return current.QuadPart;
}

Clock::~Clock()
{
}
