/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#include "Scene.h"
#include "RigidBody.h"
#include "glut.h"
#include "Shape.h"
#include <stdlib.h>
#include "DebugShapes.h"

namespace Solid
{
  static const float yThreshold = -6.0f;
  
  ObjectHandle::ObjectHandle() : object(nullptr)
  {}

  // returns true if updated, false if no need to change
  bool ObjectHandle::Update(PhysicsObject * newObject)
  {
    if (object != newObject)
    {
      object = newObject;
      return true;
    }
    return false;
  }

  Scene::Scene() : lastTime(0), currentTime(0), frameStepping(false)
  {}

  Scene::~Scene()
  {}


  void Scene::DrawArrowAtPoint(Vec2 point, Vec2 direction)
  {

  }

  void Scene::DrawBoundingBox()
  {
    std::vector<PhysicsObject> &objects = GetObjects();
    auto end = objects.end();

    // start drawing to open gl
    glColor4f(0.0f, 0.0f, 0.561f, 0.35f);
    glBegin(GL_LINES);
    for (auto i = objects.begin(); i != end; ++i)
    {
      AABB boundingBox = i->body->GetBoundingBox();

      // apply transformation to max and min
      worldToCamera.Apply(boundingBox.m_min);
      worldToCamera.Apply(boundingBox.m_max);

      // draw the verts
      glVertex2f(boundingBox.m_min.x, boundingBox.m_min.y);
      glVertex2f(boundingBox.m_min.x, boundingBox.m_max.y);

      glVertex2f(boundingBox.m_min.x, boundingBox.m_max.y);
      glVertex2f(boundingBox.m_max.x, boundingBox.m_max.y);

      glVertex2f(boundingBox.m_max.x, boundingBox.m_max.y);
      glVertex2f(boundingBox.m_max.x, boundingBox.m_min.y);

      glVertex2f(boundingBox.m_max.x, boundingBox.m_min.y);
      glVertex2f(boundingBox.m_min.x, boundingBox.m_min.y);
    }
    glEnd();

  }

  void Scene::DrawAABBTree()
  {
    std::vector<AABB> objects = RigidBody::aabbTree.GetAllAABBs();
    auto end = objects.end();

    // start drawing to open gl
    glColor4f(0.0f, 0.0f, 0.561f, 0.35f);
    glBegin(GL_LINES);
    for (auto i = objects.begin(); i != end; ++i)
    {
      AABB boundingBox = *i;

      // apply transformation to max and min
      worldToCamera.Apply(boundingBox.m_min);
      worldToCamera.Apply(boundingBox.m_max);

      // draw the verts
      glVertex2f(boundingBox.m_min.x, boundingBox.m_min.y);
      glVertex2f(boundingBox.m_min.x, boundingBox.m_max.y);

      glVertex2f(boundingBox.m_min.x, boundingBox.m_max.y);
      glVertex2f(boundingBox.m_max.x, boundingBox.m_max.y);

      glVertex2f(boundingBox.m_max.x, boundingBox.m_max.y);
      glVertex2f(boundingBox.m_max.x, boundingBox.m_min.y);

      glVertex2f(boundingBox.m_max.x, boundingBox.m_min.y);
      glVertex2f(boundingBox.m_min.x, boundingBox.m_min.y);
    }
    glEnd();

  }

  // generates a random polygon in the scene at position
  void Scene::GenerateRandomPolygonAtPosition(Vec2 translation)
  {
    // pulls a random shape from the stored shapes and puts it into the scene
    unsigned randomShapeIndex = rand() % 3;
    PhysicsObject newObj(shapes[randomShapeIndex]);
    newObj.transform.SetTranslation(translation);
    newObj.transform.lastTranslation = translation;

    // set random rotation
    float randomRot = rand() % 314 / 100.0f;
    newObj.transform.SetRotation(randomRot);
    newObj.transform.lastRotation = randomRot;

    PushNewObject(newObj);
  }

  void Scene::GenerateRandomCircleAtPosition(Vec2 translation)
  {
    // get random radius from 0.5 to 1.5
    float randomRadius = rand() % 150 / 100.0f;

    // make a circle with that radius
    Circle circle(randomRadius);

    // create the circle physics object
    PhysicsObject newObj(&circle);
    newObj.transform.SetTranslation(translation);

    // set random rotation
    //float randomRot = rand() % 314 / 100.0f;
    //newObj.transform.SetRotation(randomRot);

    PushNewObject(newObj);
  }

  // initialization
  void Scene::Initialize()
  {
    // set the last and current times
    lastTime = currentTime = clock();

    // bounding boxes should not be drawn by defualt
    drawBoundingBox = false;

    // initialize the random shapes
    Vec2 vertices1[5] = { { 0.25f, 0.25f }, { -0.38f, 0.38f },
    { -0.5f, -0.4f }, { 0, -0.5f }, { 0.45f, -0.16f } };
    shapes[0] = new Polygon(vertices1, 5);

    Vec2 vertices2[5] = { { 0.75f, 0.5f }, { 0.7f, 0.8f }, { -0.1f, 0.9f },
    { -0.6f, 0.65f }, { -0.7f, -0.5f } };
    shapes[1] = new Polygon(vertices2, 5);

    Vec2 vertices3[3] = { { 0.15f, 0.25f }, { -0.1f, -0.1f },
    { 0.2f, -0.28f } };
    shapes[2] = new Polygon(vertices3, 3);

    // initialize the floor
    PhysicsObject floor(0.5f, 0.5f);
    floor.transform.SetTranslation(Vec2(0, -2.5));
    floor.transform.SetScale(Vec2(8, 1));
    floor.body->SetFixed(true);
    PushNewObject(floor);

    // initialize the circle in the middle
    Circle circleShape(0.5f);
    PhysicsObject circle(&circleShape);
    circle.transform.AddTranslation(Vec2(0, 1));
    circle.body->SetFixed(true);
    PushNewObject(circle);

    Vec2 weirdVertices[4] = {
      { 0.37f, -0.7f },
      { -0.37f, -0.7f },
      { -0.55f, -0.9f },
      { 0.55f, -0.9f }
    };

    Polygon weirdPoly(weirdVertices, 4);
    PhysicsObject weirdShape(&weirdPoly);
    weirdShape.body->SetFixed(true);
    weirdShape.transform.SetTranslation(Vec2(0, 0));
    weirdShape.transform.SetScale(Vec2(2, 1));
    PushNewObject(weirdShape);

    // make a world to camera matrix
    worldToCamera = Matrix3(0.2f, 0.2f);
  }

  std::vector<PhysicsObject> &Scene::GetObjects()
  {
    return objects;
  }

  // renders objects using glut
  void Scene::Render()
  {
    // start drawing to open gl
    glColor4f(0.561f, 0.561f, 0.561f, 1.0f);
    glBegin(GL_LINES);

    // loop through all objects
    size_t numObjects = objects.size();
    for (size_t i = 0; i < numObjects; ++i)
    {
      // skip if inactive
      if (!objects[i].active)
        continue;

      // get the current object and draw its edges to the screen
      PhysicsObject &currentObject = objects[i];

      // if it is a polygon, draw it as a polygon
      if (currentObject.body->GetShape()->type == Shape::Type::Polygon)
      {
        // transform all verts of current object to world coords
        Polygon *poly = reinterpret_cast<Polygon *>(currentObject.body->GetShape());
        std::vector<Vec2> worldVerts(poly->vertices, poly->vertices + poly->vertexCount);

        // get the matrix from the transform and multiply by world to camera
        Matrix3 transform = worldToCamera * currentObject.transform.GetRenderMatrix3();
        // Matrix3 transform = worldToCamera * currentObject.transform.GetMatrix3();

        // apply transformation to all verts
        for (auto it = worldVerts.begin(); it != worldVerts.end(); ++it)
        {
          transform.Apply(*it);
        }

        // draw the verts
        size_t numVerts = poly->vertexCount;
        for (size_t i = 0; i < numVerts; ++i)
        {
          glVertex2f(worldVerts[i].x, worldVerts[i].y);
          glVertex2f(worldVerts[(i + 1) % numVerts].x, worldVerts[(i + 1) % numVerts].y);
        }
      }

      // draw circles
      else
      {
        // get the radius of the circle
        Circle *circle = reinterpret_cast<Circle *>(currentObject.body->GetShape());
        float radius = currentObject.transform.GetScale().x * circle->radius;
        Vec2 translation = currentObject.transform.GetTranslation();

        // apply world to camera transform
        radius *= 0.2f;
        worldToCamera.Apply(translation);

        // draw the actual circle
        unsigned numIterations = 30;
        for (unsigned j = 0; j < numIterations; ++j)
        {
          float x = radius * cos(2 * PI * j / numIterations) + translation.x;
          float y = radius * sin(2 * PI * j / numIterations) + translation.y;
          glVertex3f(x, y, 0);

          x = radius * cos(2 * PI * (j + 1) / numIterations) + translation.x;
          y = radius * sin(2 * PI * (j + 1) / numIterations) + translation.y;
          glVertex3f(x, y, 0);
        }
      }

    }
    glEnd();

    // if debug draw of bounding boxes is enabled, draw them
    if (drawBoundingBox)
      DrawAABBTree();

    glutSwapBuffers();
  }

  // push a new physics object into the scene
  void Scene::PushNewObject(PhysicsObject object)
  {
    // push to the vector of objects
    objects.push_back(object);

    // also add to the AABBtree
    RigidBody::aabbTree.AddObject(object.body);

    UpdateHandles();
  }

  // removes an object from the scene
  void Scene::RemoveObject(ObjectHandle objectHandle)
  {
    for (auto it = objects.begin(); it != objects.end(); ++it)
    {
      if (!it->active)
        continue;

      if (*it == *objectHandle.object)
      {
        //RigidBody::aabbTree.RemoveLeaf(objectHandle->body->)
        it->Destroy();
      }
    }
  }

  void Scene::CullOffScreenObjects()
  {
    for (auto it = objects.begin(); it != objects.end(); ++it)
    {
      if (!it->active)
        continue;

      if (it->transform.GetTranslation().y <= yThreshold)
      {
        it->Destroy();
      }
    }
  }

  void Scene::ClearObjects()
  {
    for (auto it = objects.begin(); it != objects.end(); ++it)
      it->Destroy();

    objects.clear();
    RigidBody::aabbTree.Clear();
  }

  // updates all handles in the objects
  void Scene::UpdateHandles()
  {
    // loop through vector and update all handles
    size_t numObjects = objects.size();
    for (size_t i = 0; i < numObjects; ++i)
    {
      // update the pointers
      PhysicsObject *newObject = &objects[i];
      newObject->UpdateHandle(newObject);
    }
  }

}