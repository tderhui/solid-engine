/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#pragma once

#include <vector>
#include <queue>
#include <functional>
#include "AABBNode.h"
#include "CollisionPair.h"
#include "Scene.h"

namespace Solid
{
  class RigidBody;

  typedef std::vector<CollisionPair> CollisionPairs;

  struct ChildParentSet
  {
    ChildParentSet() : m_child(-1), m_parent(-1), m_newBranch(-1) {}
    int m_child;
    int m_parent;
    int m_newBranch;
  };

  class AABBTree
  {
  public:
    AABBTree();
    ~AABBTree();

    ObjectHandle QueryPoint(Vec2 point);

    void Update(); 
    void GetCollisions();
    AABBNode &AddObject(RigidBody *object);
    void RemoveLeaf(int index);

    AABBNode &GetRoot();
    int GetRootIndex();

    // clears the tree
    void Clear();

    static int GetTreeHeight(AABBNode &tree);

    // gets all AABBs in the tree
    std::vector<AABB> GetAllAABBs();

    std::vector<AABBNode> m_nodes;

    // internal collision pairs
    CollisionPairs m_collisionPairs;
    
    // single invalid node used to check if returned node is valid
    static AABBNode s_invalidNode;
    
  private:
    // recursive query point helper
    ObjectHandle QueryPointHelper(AABBNode &tree, Vec2 point);

    // clears the childrenCompared flag
    void ClearComparedFlags();

    // adds a rigid body pair to the list of collisions
    void AddBodyPair(RigidBody *bodyA, RigidBody *bodyB);

    // add all combinations of rigid bodies in a tree to the list of collisions
    // mainly used for debugging
    void AddAllBodiesToCollisionList(AABBNode &tree);
    
    // helper function to add children to collision list
    void CompareChildren(AABBNode &tree);

    // recursive helper function to add all bodies to collision list
    void AddAllBodiesHelper(AABBNode &node1, AABBNode &node2);

    // recursive helper function to get collision pairs
    void GetCollisionsHelper(AABBNode &node1, AABBNode &node2);

    // recursive helper function to get all AABBS
    void GetAllAABBsHelper(AABBNode &tree, std::vector<AABB> &result);

    // recursive helper function to insert a new node
    // current heuristic is least volume increase
    AABBNode &InsertHelper(AABBNode &tree, AABBNode &newNode);

    // inserts a node into the vector and returns its index
    int Insert(AABBNode &node);

    void FindInvalidNodes(AABBNode &node);

    // this function does NOT add to the free list
    ChildParentSet RemoveLeafHelper(int index);
    
    int m_rootIndex;

    // performance info
    int m_numDuplicates;

    // using std::greater will make sure that smaller indexes are pulled first
    std::priority_queue<int, std::vector<int>, std::greater<int> > m_freeList;
    
    // vector of invalid node indices
    std::vector<int> m_invalidNodeIndices;

  };
}