/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#include "Transform.h"
#include "RigidBody.h"

namespace Solid
{
  // set everything to zero
  Transform::Transform() : transformMatrix(), hasChanged(false), translation(),
    lastTranslation(), scale(1, 1), lastScale(1, 1), rotation(0), lastRotation(0)
  {}

  // destructor does nothing
  Transform::~Transform()
  {}

  // gettors for transform
  Vec2 Transform::GetTranslation()
  {
    return translation;
  }

  Vec2 Transform::GetScale()
  {
    return scale;
  }
  
  float Transform::GetRotation()
  {
    return rotation;
  }

  // return matrix. if transform has changed since last frame, recalculate
  Matrix3 Transform::GetMatrix3()
  {
    if (hasChanged)
      return Matrix3(translation) * Matrix3(rotation) * Matrix3(scale.x, scale.y);
    else
      return transformMatrix;
  }

  // return matrix but tweened
  Matrix3 Transform::GetRenderMatrix3()
  {
    float alpha = RigidBody::accumulator / RigidBody::physicsTimestep;
    float inverseAlpha = 1 - alpha;

    Vec2 finalScale = lastScale * inverseAlpha + scale * alpha;
    Vec2 finalTranslation = lastTranslation * inverseAlpha + translation * alpha;
    float finalRotation = lastRotation * inverseAlpha + rotation * alpha;

    return Matrix3(finalTranslation) * Matrix3(finalRotation) * Matrix3(finalScale.x, finalScale.y);
  }

  // settors for transform
  void Transform::SetTranslation(Vec2 newTrans)
  {
    translation = newTrans;
    hasChanged = true;
  }

  void Transform::SetScale(Vec2 newScale)
  {
    scale = newScale;
    hasChanged = true;
  }

  void Transform::SetRotation(float rads)
  {
    rotation = rads;
    hasChanged = true;
  }

  void Transform::AddTranslation(Vec2 newTrans)
  {
    translation += newTrans;
    hasChanged = true;
  }

  void Transform::AddRotation(float rads)
  {
    rotation += rads;
    hasChanged = true;
  }

}