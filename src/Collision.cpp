/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#include "RigidBody.h"
#include "Shape.h"
#include "Manifold.h"
#include "Vec2.h"
#include <algorithm>
#include "DebugShapes.h"

#define DEBUG_DRAW 1


namespace Solid
{
  static const float EPSILON = 0.00001f;
  static const float CORRECTION = 0.4f;
  static const float min_magnitude = 0.0f;

  /******************************************************************
  * Collision Resolution Functions                                   *
  ******************************************************************/

  bool RigidBody::Contains(Vec2 point)
  {
    return true;
  }

  unsigned RigidBody::Clip(Vec2 referenceNormal, float distOriginToFace, Face &inputFace, Face &contactPoints)
  {
    unsigned numContacts = 0;
    Face ret;

    // Retrieve distances from each endpoint to the line
    float distance1 = inputFace.point1 * referenceNormal - distOriginToFace;
    float distance2 = inputFace.point2 * referenceNormal - distOriginToFace;

    // If negative (behind plane) clip
    if (distance1 < 0)
      contactPoints[numContacts++] = inputFace.point1;
    if (distance2 < 0)
      contactPoints[numContacts++] = inputFace.point2;

    // If the points are on different sides of the plane
    if (distance1 * distance2 < 0)
    {
      Vec2 inputFaceVector = inputFace.findVector();
      float alpha = distance1 / (distance1 - distance2);

      // push the intersection point
      Vec2 intersectionPoint = inputFace.point1 + (inputFaceVector * alpha);
      contactPoints[numContacts++] = intersectionPoint;
    }

    if (numContacts >= 3)
      throw ("Invalid Contact Points!");

    return numContacts;
  }

  float RigidBody::findAxisOfLeastPenetration(const RigidBody &rhs, Manifold &result) const
  {
#if _DEBUG
    std::string name = GetOwner()->name;
    std::string rhsName = rhs.GetOwner()->name;
#endif
    // cast the shape pointers to polygon pointers
    Polygon *left = reinterpret_cast<Polygon *>(GetShape());
    Polygon *right = reinterpret_cast<Polygon *>(rhs.GetShape());

    // get the parents' transforms
    Matrix3 leftTransform = GetOwner()->transform.GetMatrix3();
    Matrix3 rightTransform = rhs.GetOwner()->transform.GetMatrix3();

    float smallestPenetration = 10000000.0f;
    int smallestAxis = 0;

    // make a rotation matrix using the object's rotation to rotate normals
    Matrix3 rot(GetOwner()->transform.GetRotation());

    Vec2 leftCenter = leftTransform * left->center;
    Vec2 rightCenter = rightTransform * right->center;
    Vec2 relativePosition = rightCenter - leftCenter;

    // debug draw the centers
    Point::Draw(leftCenter);
    Point::Draw(rightCenter);

    // loop through all axes
    for (unsigned i = 0; i < left->vertexCount; ++i)
    {
      Vec2 normal = left->normals[i];
      normal = rot * normal;

      // if normal is pointing AWAY from other shape, skip this side
      if (normal * relativePosition < 0)
        continue;

      // loop through both polygons' corners to find the max/min projections
      float box1_maxProjection = -1000000.0f;
      float box1_minProjection = 1000000.0f;

      // start with left polygon
      for (unsigned j = 0; j < left->vertexCount; ++j)
      {
        float currentProjection = leftTransform * left->vertices[j] * normal;

        if (currentProjection < box1_minProjection)
          box1_minProjection = currentProjection;

        if (currentProjection > box1_maxProjection)
          box1_maxProjection = currentProjection;
      }

      // now the right polygon
      float box2_maxProjection = -1000000.0f;
      float box2_minProjection = 1000000.0f;

      for (unsigned j = 0; j < right->vertexCount; ++j)
      {
        float currentProjection = rightTransform * right->vertices[j] * normal;

        if (currentProjection < box2_minProjection)
          box2_minProjection = currentProjection;

        if (currentProjection > box2_maxProjection)
          box2_maxProjection = currentProjection;
      }

      // if no collision, return false
      if (box2_maxProjection < box1_minProjection || box1_maxProjection < box2_minProjection)
      {
        result.collision = false;
        return 0;
      }

      // if yes collision, check for minimum penetration
      float penetration1 = box1_maxProjection - box2_minProjection;
      float penetration2 = box2_maxProjection - box1_minProjection;
      float currentPenetration = penetration1 < penetration2 ? penetration1 : penetration2;

      if (currentPenetration < smallestPenetration)
      {
        smallestPenetration = currentPenetration;
        smallestAxis = i;
      }
    }

    // if all axes show collision, then there is collision
    result.collision = true;
    result.collisionDepth = smallestPenetration;
    result.collisionNormal = rot * left->normals[smallestAxis];
    return smallestPenetration;
  }

  bool RigidBody::findReferenceFace(Face &referenceFace, Vec2 collisionNormal)
  {
    Polygon *boxShape = reinterpret_cast<Polygon *>(GetShape());

    // get the transformation matrix and position
    Matrix3 transform = this->owner->transform.GetMatrix3();
    Vec2 center = this->owner->transform.GetMatrix3() * boxShape->center;

    // find number of faces
    unsigned int numFaces = boxShape->vertexCount;

    // loop through for the number of faces
    for (unsigned i = 0; i < numFaces; ++i)
    {
      // transform the current face by the object's transformation
      Face currentFace = { transform * boxShape->vertices[i], transform * boxShape->vertices[(i + 1) % numFaces] };
      Vec2 faceVector = currentFace.point2 - currentFace.point1;

      // test each face to see if it's orthogonal to the normal

      // if it's not orthogonal, skip the other steps
      float testValue = faceVector * collisionNormal;
      if (testValue <= -EPSILON || testValue >= EPSILON)
      {
        continue;
      }

      // if it is orthogonal to the normal, test to see if the normal is pointing towards the face
      Vec2 centerToPoint = currentFace.point1 - center;

      // if it is orthogonal and normal points towards it, return it as the reference face
      if (centerToPoint * collisionNormal > 0)
      {
        referenceFace = currentFace;

        return true;
      }

    }

    // if reference face unable to be found
    return false;
  }

  bool RigidBody::findIncidentFace(Face &incidentFace, const Face &referenceFace, Vec2 collisionNormal)
  {
    // cast the appropriate pointers
    Polygon *boxShape = reinterpret_cast<Polygon *>(GetShape());

    // find reference normal
    Vec2 referenceNormal = referenceFace.findNormal();

    unsigned vertexCount = boxShape->vertexCount;

    int correctIndex = -1;
    float largestProjection = -100000.0f;

    // loop through each normal of the body
    for (unsigned i = 0; i < vertexCount; ++i)
    {
      // get the rotation matrix
      Matrix3 rot(this->GetOwner()->transform.GetRotation());

      // get the vector that points away from each normal
      Vec2 negNormal = rot * boxShape->normals[i] * -1;

      // if the negNormal points most towards the collision normal, it is the incident face
      float currentProjection = negNormal * referenceNormal;

      if (currentProjection > largestProjection)
      {
        largestProjection = currentProjection;
        correctIndex = i;
      }
    }

    // if no incident face was found
    if (correctIndex == -1)
    {
      return false;
    }

    Matrix3 transform = this->owner->transform.GetMatrix3();
    incidentFace.point1 = transform * boxShape->vertices[correctIndex];
    incidentFace.point2 = transform * boxShape->vertices[(correctIndex + 1) % vertexCount];
    
    return true;
  }

  /******************************************************************
  * Collision Detection Functions                                   *
  ******************************************************************/

  // calls the appropriate collision function based on shape type
  void RigidBody::ResolveCollision(Manifold &result, RigidBody &rhs)
  {
    // polygon to polygon collision
    if (shape->GetType() == Shape::Type::Polygon && rhs.shape->GetType() == Shape::Type::Polygon)
    {
      //Manifold GJKResult;
      //TestGJKCollision(result, rhs);
      PolygonToPolygon(result, rhs);

      //if (GJKResult.contacts[0] != result.contacts[0] && GJKResult.contacts[0] != result.contacts[1])
      //  printf("hello");
      return;
    }

    // polygon to circle collision
    else if (shape->GetType() == Shape::Type::Polygon && rhs.shape->GetType() == Shape::Type::Circle)
    {
      PolygonToCircle(result, rhs);
      return;
    }

    // circle to polygon collision
    else if (shape->GetType() == Shape::Type::Circle && rhs.shape->GetType() == Shape::Type::Polygon)
    {
      CircleToPolygon(result, rhs);
      return;
    }

    // circle to circle collision
    else
    {
      CircleToCircle(result, rhs);
      return;
    }

    result.collision = false;
  }

  void RigidBody::PolygonToPolygon(Manifold &result, RigidBody &rhs)
  {
#if _DEBUG
    std::string name = GetOwner()->name;
    std::string rhsName = rhs.GetOwner()->name;
#endif
    Manifold resultA;
    float penetrationA = findAxisOfLeastPenetration(rhs, resultA);

    if (resultA.collision == false)
    {
      result.collision = false;
      return;
    }

    Manifold resultB;
    float penetrationB = rhs.findAxisOfLeastPenetration(*this, resultB);

    if (resultB.collision == false)
    {
      result.collision = false;
      return;
    }

    findAxisOfLeastPenetration(rhs, resultA);

    // if we should use object A to B
    if (penetrationA <= penetrationB)
    {
      result = resultA;
      Face incidentFace, referenceFace;
      findReferenceFace(referenceFace, result.collisionNormal);
      rhs.findIncidentFace(incidentFace, referenceFace, result.collisionNormal);

      // side plane normal
      Vec2 sidePlaneNormal = referenceFace.findVector();
      sidePlaneNormal.Normalize();

      // find the dot products of the reference face's vertices
      float point1Projection = referenceFace.point1 * sidePlaneNormal * -1.0f;
      float point2Projection = referenceFace.point2 * sidePlaneNormal;

      Face clippedFace;

      // clip the incident face to the left side plane
      Clip(sidePlaneNormal, point2Projection, incidentFace, clippedFace);

      // clip the incident face to the right
      incidentFace = clippedFace;
      Clip(sidePlaneNormal * -1.0f, point1Projection, incidentFace, clippedFace);

      Vec2 referenceNormal = referenceFace.findNormal();
      referenceNormal.Normalize();
      float distanceOriginToFace = referenceFace.point1 * referenceNormal;

      Face contactPoints;

      unsigned numContacts = Clip(referenceNormal, distanceOriginToFace, clippedFace, contactPoints);

      if (numContacts)
      {
        for (unsigned i = 0; i < numContacts; ++i)
        {
          Point::Draw(contactPoints[i]);
          Arrow::Draw(contactPoints[i], result.collisionNormal);
          result.contacts[i] = contactPoints[i];
        }
        result.contactCount = numContacts;
      }

      return;
    }

    // if we should use object B to A
    else
    {
      result = resultB;
      Face incidentFace, referenceFace;
      rhs.findReferenceFace(referenceFace, result.collisionNormal);
      findIncidentFace(incidentFace, referenceFace, result.collisionNormal);

      // side plane normal
      Vec2 sidePlaneNormal = referenceFace.findVector();
      sidePlaneNormal.Normalize();

      // find the dot products of the reference face's vertices
      float point1Projection = referenceFace.point1 * sidePlaneNormal * -1.0f;
      float point2Projection = referenceFace.point2 * sidePlaneNormal;

      Face clippedFace;

      // clip the incident face to the left side plane
      Clip(sidePlaneNormal, point2Projection, incidentFace, clippedFace);

      // clip the incident face to the right
      incidentFace = clippedFace;
      Clip(sidePlaneNormal * -1.0f, point1Projection, incidentFace, clippedFace);

      Vec2 referenceNormal = referenceFace.findNormal();
      referenceNormal.Normalize();
      float distanceOriginToFace = referenceFace.point1 * referenceNormal;

      Face contactPoints;

      unsigned numContacts = Clip(referenceNormal, distanceOriginToFace, clippedFace, contactPoints);

      if (numContacts)
      {
        for (unsigned i = 0; i < numContacts; ++i)
        {
          Point::Draw(contactPoints[i]);
          Arrow::Draw(contactPoints[i], result.collisionNormal);
          result.contacts[i] = contactPoints[i];
        }
        result.contactCount = numContacts;
      }

      result.flip = true;
      return;
    }

  }

  void RigidBody::PolygonToCircle(Manifold &result, RigidBody &rhs)
  {
    // cast the shape pointers to polygon pointers
    Polygon *left = reinterpret_cast<Polygon *>(GetShape());
    Circle *right = reinterpret_cast<Circle *>(rhs.GetShape());

    // find which face the circle is closest to

    // make an array of transformed vertices
    Matrix3 leftTransform = GetOwner()->transform.GetMatrix3();
    Vec2 transformedVertices[MaxVertexCount];

    unsigned vertexCount = left->vertexCount;

    for (unsigned i = 0; i < vertexCount; ++i)
    {
      transformedVertices[i] = leftTransform * left->vertices[i];
    }

    // make an array of transformed faces
    Face transformedFaces[MaxVertexCount];

    for (unsigned i = 0; i < vertexCount; ++i)
    {
      transformedFaces[i] = Face(transformedVertices[i], transformedVertices[(i + 1) % vertexCount]);
    }

    // loop through each face and find smallest distance

    int smallestIndex = -1;
    float maxSeparation = -10000.0f;

    Vec2 circleCenter = rhs.GetOwner()->transform.GetTranslation();
    Matrix3 rot(GetOwner()->transform.GetRotation());

    for (unsigned i = 0; i < vertexCount; ++i)
    {
      Vec2 cornerToCircle = circleCenter - transformedVertices[i];
      float separation = cornerToCircle * (rot * left->normals[i]);

      if (separation > maxSeparation)
      {
        smallestIndex = int(i);
        maxSeparation = separation;
      }
    }

    // if could not find smallest index
    if (smallestIndex == -1)
    {
      result.collision = false;
      return;
    }

    // calculate the projection of each vertexToCircle onto the faceVector
    Face closestFace = transformedFaces[smallestIndex];

    Vec2 faceVector = closestFace.point2 - closestFace.point1;
    Vec2 leftVertexToCircle = circleCenter - closestFace.point2;
    Vec2 rightVertexToCircle = circleCenter - closestFace.point1;

    float leftProjection = leftVertexToCircle * faceVector;
    float rightProjection = rightVertexToCircle * faceVector;

    result.collision = false;

    // if the circle is in the left edge region
    if (leftProjection > 0)
    {
      float radius = rhs.owner->transform.GetScale().x * right->radius;
      result.collision = true;
      result.collisionNormal = leftVertexToCircle;
      result.collisionNormal.Normalize();
      result.collisionDepth = radius - leftVertexToCircle * result.collisionNormal;
      if (result.collisionDepth < 0)
      {
        result.collision = false;
        return;
      }

#if _DEBUG
      std::string name = GetOwner()->name;
      std::string rhsName = rhs.GetOwner()->name;
#endif

      result.contactCount = 1;
      result.contacts[0] = closestFace.point2;
    }

    // if the circle is in the right edge region
    else if (rightProjection < 0)
    {
      float radius = rhs.owner->transform.GetScale().x * right->radius;
      result.collision = true;
      result.collisionNormal = rightVertexToCircle;
      result.collisionNormal.Normalize();
      float depth = radius - rightVertexToCircle.Length();
      result.collisionDepth = depth;
      if (depth < 0)
      {
        result.collision = false;
        return;
      }

#if _DEBUG
      std::string name = GetOwner()->name;
      std::string rhsName = rhs.GetOwner()->name;
#endif

      result.contactCount = 1;
      result.contacts[0] = closestFace.point1;
    }


    // if the circle is in the face region
    else
    {
      float radius = rhs.owner->transform.GetScale().x * right->radius;
      result.collision = true;
      result.collisionNormal = closestFace.findNormal();
      result.collisionNormal.Normalize();
      result.collisionDepth = radius - leftVertexToCircle * result.collisionNormal;
      if (result.collisionDepth < 0)
      {
        result.collision = false;
        return;
      }

#if _DEBUG
      std::string name = GetOwner()->name;
      std::string rhsName = rhs.GetOwner()->name;
#endif

      result.contactCount = 1;
      float alpha = rightProjection / (rightProjection - leftProjection);
      result.contacts[0] = closestFace.point1 + (faceVector * alpha);
      float gamma = 0.66f;

      Point::Draw(result.contacts[0]);
      Arrow::Draw(result.contacts[0], result.collisionNormal);
    }

  }

  void RigidBody::CircleToPolygon(Manifold &result, RigidBody &rhs)
  {
    rhs.PolygonToCircle(result, *this);
    result.flip = true;
  }

  void RigidBody::CircleToCircle(Manifold &result, RigidBody &rhs)
  {
#if _DEBUG
    std::string name = GetOwner()->name;
    std::string rhsName = rhs.GetOwner()->name;
#endif
    // get normal
    Vec2 normal = rhs.owner->transform.GetTranslation() - owner->transform.GetTranslation();

    // get distance between circles
    float squareDistanceBetween = normal.LengthSquared();

    // get the radii multiplied by scale
    float leftRadius = owner->transform.GetScale().x * ((Circle *)GetShape())->radius;
    float rightRadius = rhs.owner->transform.GetScale().x * ((Circle *)rhs.GetShape())->radius;

    // if no collision
    if (squareDistanceBetween > (leftRadius + rightRadius) * (leftRadius + rightRadius))
    {
      result.collision = false;
      return;
    }

    // if there is collision
    float distance = sqrt(squareDistanceBetween);

    result.collisionDepth = leftRadius + rightRadius - distance;
    result.collisionNormal = normal / distance; // normalize the normal
    result.contactCount = 1;
    result.contacts[0] = owner->transform.GetTranslation() + result.collisionNormal * leftRadius;
    result.flip = false;
    result.collision = true;

    Point::Draw(result.contacts[0]);
    Arrow::Draw(result.contacts[0], result.collisionNormal);
  }
}