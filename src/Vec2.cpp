/******************************************************************************

Copyright 2015 Tai Der Hui. All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.

******************************************************************************/

#include <math.h>
#include "Vec2.h"

namespace Solid
{
  Vec2::Vec2() : x(0), y(0) {}

  Vec2::Vec2(float x_, float y_) : x(x_), y(y_) {}

  Vec2::Vec2(float theta) : x(cosf(theta)), y(sinf(theta)) {}

  void Vec2::Set(float x_, float y_)
  {
    x = x_;
    y = y_;
  }

  // normalizes the vector
  void Vec2::Normalize()
  {
    float length = this->Length();

    if (length == 0)
      return;

    x /= length;
    y /= length;
  }

  // returns a new normalized vector
  Vec2 Vec2::Normalized()
  {
    float length = this->Length();

    if (length == 0)
      return Vec2(0, 0);

    Vec2 ret(x / length, y / length);
    return ret;
  }

  float Vec2::Length()
  {
    return std::sqrt(x * x + y * y);
  }

  float Vec2::LengthSquared()
  {
    return x * x + y * y;
  }

  float Vec2::GetAngle() const
  {
    return tanf(y / x);
  }

  float Vec2::Cross(const Vec2 &rhs)
  {
    return (x * rhs.y - y * rhs.x);
  }

  Vec2 Vec2::Midpoint(Vec2 &rhs)
  {
    return Vec2((x + rhs.x) / 2.0f, (y + rhs.y) / 2.0f);
  }


  Vec2 Vec2::operator+(const Vec2 &rhs) const
  {
    Vec2 ret(x + rhs.x, y + rhs.y);
    return ret;
  }

  Vec2 &Vec2::operator+=(const Vec2 &rhs)
  {
    x += rhs.x;
    y += rhs.y;

    return *this;
  }

  Vec2 Vec2::operator-(const Vec2 &rhs) const
  {
    Vec2 ret(x - rhs.x, y - rhs.y);
    return ret;
  }

  Vec2 &Vec2::operator-=(const Vec2 &rhs)
  {
    x -= rhs.x;
    y -= rhs.y;

    return *this;
  }

  // Vector Scaling
  Vec2 Vec2::operator*(float scalar) const
  {
    return Vec2(x * scalar, y * scalar);
  }

  // Dot product
  float Vec2::operator*(const Vec2 &rhs) const
  {
    return x * rhs.x + y * rhs.y;
  }

  // Vector Scaling
  Vec2 &Vec2::operator*=(float scalar)
  {
    x *= scalar;
    y *= scalar;
    return *this;
  }

  Vec2 Vec2::operator/(float divisor) const
  {
    if (divisor == 0)
      return Vec2(0, 0);
      //throw("Divide by Zero!");

    return Vec2(x / divisor, y / divisor);
  }

  Vec2 &Vec2::operator/=(float divisor)
  {
    if (divisor == 0)
      throw("Divide by Zero!");

    x /= divisor;
    y /= divisor;

    return *this;
  }

  bool Vec2::operator==(const Vec2 &rhs) const
  {
    return (x == rhs.x && y == rhs.y);
  }

  // first sorts by x, then y
  bool Vec2::operator>(const Vec2 &rhs) const
  {
    if (x == rhs.x)
    {
      return y > rhs.y;
    }
    else
      return x > rhs.x;
  }

  bool Vec2::operator<(const Vec2 &rhs) const
  {
    if (x == rhs.x)
    {
      return y < rhs.y;
    }
    else
      return x < rhs.x;
  }

  bool Vec2::operator!=(const Vec2 &rhs) const
  {
    return (x != rhs.x || y != rhs.y);
  }

  std::ostream &operator<<(std::ostream &os, const Vec2 &input)
  {
    os << input.x << ", " << input.y;

    return os;
  }

  Vec2 Cross(float scalar, const Vec2 &vector)
  {
    return Vec2(-scalar * vector.y, scalar * vector.x);
  }
  
  Vec2 Cross(const Vec2 &vector, float scalar)
  {
    return Vec2(scalar * vector.y, -scalar * vector.x);
  }
}